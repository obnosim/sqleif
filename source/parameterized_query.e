note
	description: "SQLite3 prepared statement that accepts bound operands and returns typed results"
	author: "obnosim"

class
	PARAMETERIZED_QUERY [ARGS -> detachable TUPLE create default_create end, ROW -> attached TUPLE [detachable ANY] create default_create end]

inherit

	QUERY [ROW]
		rename
			scalar as scalar_as_bound,
			single as single_as_bound,
			single_or_default as single_or_default_as_bound,
			first as first_as_bound,
			first_or_default as first_or_default_as_bound,
			list as list_as_bound
		undefine
			out
		redefine
			make_with_flag
		select
			start
		end

	PARAMETERIZED_STATEMENT [ARGS]
		rename
			step as forth
		undefine
			forth, after
		redefine
			make_with_flag
		end

create
	make, make_long_lived

feature {NONE} -- Initialization

	make_with_flag (a_database: DATABASE; a_query: READABLE_STRING_8; a_flags: like sqlite_prepare_flag_persistent)
			-- <Precursor>
		do
			precursor {QUERY} (a_database, a_query, a_flags)
		end

feature -- Access

	first_or_default (a_args: attached ARGS): detachable ROW
			-- The single result when called with `a_args', if any
		do
			call (a_args)
			if success then
				result := item
			end
		end

	first (a_args: attached ARGS): ROW
			-- The first result when called with `a_args'
		do
			check attached first_or_default (a_args) as l_first then
				result := l_first
			end
		end

	single_or_default (a_args: attached ARGS): detachable ROW
			-- The single result when called with `a_args', if any
		do
			result := first_or_default (a_args)
			if success then
				forth
			end
		ensure
			end_of_results: exhausted
		end

	single, scalar (a_args: attached ARGS): ROW
			-- The single result when called with `a_args'
		do
			check attached single_or_default (a_args) as l_single then
				result := l_single
			end
		ensure
			end_of_results: exhausted
		end

	list (a_args: attached ARGS): ARRAYED_LIST [ROW]
			-- All result rows when called with `a_args'
		do
			bind (a_args)
			create result.make_from_iterable (current)
		ensure
			all_read: exhausted
		end

end
