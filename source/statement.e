note
	description: "Base wrapper for SQLite3 statements"
	author: "obnosim"

class
	STATEMENT

inherit

	DISPOSABLE
		rename
			twin as any_twin
		export {NONE} all
		redefine
			out
		end

	CONFIGURABLE
		rename
			store as converters,
			twin as any_twin
		redefine
			out
		end

	C_STRING_CONVERSION
		rename
			twin as any_twin
		export {NONE} all
		redefine
			out
		end

	RETURN_CODES
		rename
			twin as any_twin
		export {NONE} all
		redefine
			out
		end

create
	make, make_long_lived

feature {STATEMENT} -- Initialization

	make (a_database: DATABASE; a_statement: READABLE_STRING_8)
			-- Creates `internal_item' from `a_statement' in `a_database'.
			-- Optimized for short life cycles.
		do
			make_with_flag (a_database, a_statement, 0)
		end

	make_long_lived (a_database: DATABASE; a_statement: READABLE_STRING_8)
			-- Creates `internal_item' from `a_statement' in `a_database'.
			-- Optimized for longer life cycles.
		do
			make_with_flag (a_database, a_statement, sqlite_prepare_flag_persistent)
			long_lived := true
		ensure
			long_lived
		end

	make_with_flag (a_database: DATABASE; a_statement: READABLE_STRING_8; a_flags: NATURAL)
			-- Creates `internal_item' from `a_statement' in `a_database' using `a_flags'.
		local
			l_c_statement: C_STRING
		do
			database := a_database
			if attached {IMMUTABLE_STRING_8} a_statement as la_statement then
				statement := la_statement
			else
				create statement.make_from_string (a_statement)
			end
			last_return_code := last_return_code.default
			l_c_statement := readable_string_8_to_c (statement)
			create internal_item
			handle_error (c_prepare_statement (l_c_statement.item, l_c_statement.count, a_flags))
		ensure
			internal_item: internal_item /= default_pointer
		end

	c_prepare_statement (a_c_statement: like {C_STRING}.item; a_c_statement_length: like {C_STRING}.count; a_flags: NATURAL): INTEGER
			-- Calls `sqlite3_prepare_v3' with the correct arguments
		local
			l_tail: POINTER
		do
			result := sqlite3_prepare_v3 (database, a_c_statement, a_c_statement_length, a_flags, $internal_item, $l_tail)
		end

	frozen sqlite3_prepare_v3 (a_database: like {DATABASE}.internal_item; a_query: like {C_STRING}.item; a_query_length: like {C_STRING}.count; a_flags: NATURAL; a_statement_out: TYPED_POINTER [like internal_item]; a_next_out: detachable TYPED_POINTER [POINTER]): INTEGER
			-- Instantiates into `a_statement_out' a new prepared statement executing `a_query' in `a_database' with `a_flags'
		external
			"C inline use <sqlite3.h>"
		alias
			"[
				sqlite3_stmt** stmt = (sqlite3_stmt**)$a_statement_out;
				const char** tail = (const char**)$a_next_out;
				return sqlite3_prepare_v3($a_database, $a_query, $a_query_length, $a_flags, stmt, tail);
			]"
		end

	frozen sqlite_prepare_flag_persistent: NATURAL
			-- Creation flag telling SQLite3 that the statement is expected to be used several times.
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_PREPARE_PERSISTENT"
		end

feature -- Output

	out: STRING_8
			-- <Precursor>
		do
			result := statement.to_string_8
		end

feature {STATEMENT} -- Implementation

	internal_item: POINTER
			-- SQLite prepared statement object pointer

	last_return_code: INTEGER
			-- Return code of the last executed step of `Current'

	database: DATABASE
			-- Reference to the target database from which `Current' was created, to ensure it is not disposed before `Current' is

	statement: IMMUTABLE_STRING_8
			-- The string statement used to create `Current', for copying and debugging purposes

	long_lived: BOOLEAN
			-- Whether `Current' was created in a way that is optimized for reuse, for copying purposes

	converters: TYPE_CONVERTERS assign share_converters
			-- Custom converters, specific to `Current'.
			-- Takes precedence over the database's.
		attribute
			create result
		end

	share_converters (a_converters: like converters)
			-- Assigns `a_converters' to `converters'
		do
			converters := a_converters
		ensure
			shared: converters = a_converters
		end

	handle_error (a_code: like last_return_code)
		do
			if a_code /= success_code and a_code /= more_rows and a_code /= rows_exhausted then
				database.handle_error (a_code)
			end
		end

feature {NONE} -- Disposal

	dispose
			-- Frees `internal_item'
		do
			sqlite3_finalize (internal_item).do_nothing
		end

	frozen sqlite3_finalize (a_item: like internal_item): INTEGER
			-- Correctly frees the memory used by the prepared statement `a_item'
		external
			"C (sqlite3_stmt*) : int | <sqlite3.h>"
		end

feature -- Cloning

	twin: like Current
			-- An independant copy of `Current'
		do
			if long_lived then
				result := long_lived_twin
			else
				result := short_lived_twin
			end
		ensure
			same_configuration: result.converters = converters
		end

	long_lived_twin: like Current
			-- An independant copy of `Current', optimized for reuse
		do
			create result.make_long_lived (database, statement)
			result.share_converters (converters)
		ensure
			same_configuration: result.converters = converters
		end

	short_lived_twin: like Current
			-- An independant copy of `Current', optimized for a single use
		do
			create result.make (database, statement)
			result.share_converters (converters)
		ensure
			same_configuration: result.converters = converters
		end

feature -- Access

	reset
			-- Resets `Current' so it can be applied again.
			-- Has no effect on the configuration.
		do
			sqlite3_reset (internal_item).do_nothing
			last_return_code := last_return_code.default
		ensure
			internal_item: internal_item /= default_pointer
			reset: not after
		end

	apply
			-- Applies `Current' anew with the same configuration.
		do
			reset
			step
		ensure then
			no_error: last_return_code = success_code or last_return_code = more_rows or last_return_code = rows_exhausted
		end

	step
			-- Executes one step of `Current'
		do
			last_return_code := sqlite3_step (internal_item)
			handle_error (last_return_code)
		ensure then
			no_error: last_return_code = success_code or last_return_code = more_rows or last_return_code = rows_exhausted
		end

	after: BOOLEAN
			-- Whether `Current' has reached the end
		do
			result := last_return_code = rows_exhausted
		end

feature {NONE} -- Execution

	frozen sqlite3_reset (a_item: like internal_item): INTEGER
			-- Resets the prepared statement `a_item' so it can be reexecuted
		external
			"C (sqlite3_stmt*) : int | <sqlite3.h>"
		end

	frozen sqlite3_step (a_item: like internal_item): INTEGER
			-- Executes one step of `a_item'
		external
			"C (sqlite3_stmt*) : int | <sqlite3.h>"
		end

end
