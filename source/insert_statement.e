note
	description: "Wrapper to execute SQLite3 insert statements"
	author: "obnosim"

class
	INSERT_STATEMENT [ARGS -> attached TUPLE [detachable ANY] create default_create end]

inherit

	PARAMETERIZED_STATEMENT [ARGS]
		rename
			make as make_statement,
			make_long_lived as make_long_lived_statement
		redefine
			long_lived_twin,
			short_lived_twin
		end

create {INSERT_STATEMENT}
	make_statement, make_long_lived_statement

create
	make, make_long_lived

feature {NONE} -- Initialization

	create_statement (a_table: READABLE_STRING_8; a_columns: READABLE_INDEXABLE [READABLE_STRING_8]): STRING
			-- Prepared SQL statement to insert into table `a_table' the values `a_columns'
		require
			matching_operand_counts: (1 + a_columns.upper - a_columns.lower) = operand_count
		local
			i, j: INTEGER
		do
			result := "INSERT INTO "
			result.append (a_table)
			result.append (" (")
			from
				i := a_columns.lower
			until
				i > a_columns.upper
			loop
				result.append (a_columns.item (i))
				i := i + 1
				if i <= a_columns.upper then
					result.append (", ")
				end
			end
			result.append (") VALUES (")
			from
				i := a_columns.lower
				j := 1
			until
				i > a_columns.upper
			loop
				result.extend ('?')
				result.append (j.out)
				i := i + 1
				j := j + 1
				if i <= a_columns.upper then
					result.append (", ")
				end
			end
			result.append (");")
		end

	make (a_database: DATABASE; a_table: READABLE_STRING_8; a_columns: READABLE_INDEXABLE [READABLE_STRING_8])
			-- Creates `internal_item' from `a_statement' in `a_database'.
			-- Optimized for short life cycles.
		do
			make_statement (a_database, create_statement (a_table, a_columns))
		end

	make_long_lived (a_database: DATABASE; a_table: READABLE_STRING_8; a_columns: READABLE_INDEXABLE [READABLE_STRING_8])
			-- Creates `internal_item' from `a_statement' in `a_database'.
			-- Optimized for longer life cycles.
		do
			make_long_lived_statement (a_database, create_statement (a_table, a_columns))
		end

feature -- Cloning

	long_lived_twin: like Current
			-- <Precursor>
		do
			create result.make_long_lived_statement (database, statement)
		end

	short_lived_twin: like Current
			-- <Precursor>
		do
			create result.make_statement (database, statement)
		end

end
