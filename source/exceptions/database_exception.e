note
	description: "SQlite3 database exception"
	author: "obnosim"

class
	DATABASE_EXCEPTION

inherit

	DEVELOPER_EXCEPTION
		redefine
			tag
		end

create
	make, make_from_c

feature {NONE} -- Initialization

	make (a_error_code: like {RETURN_CODES}.generic_error; a_error_string, a_error_message: READABLE_STRING)
			-- `a_error_code': SQLite3 return code
			-- `a_error_string': Generic message associated with `a_error_code'
			-- `a_error_message': Specific error message
		do
			set_description ("Error code " + a_error_code.out + ": " + a_error_string + ": " + a_error_message)
		ensure
			description: attached description
		end

	make_from_c (a_error_code: like {RETURN_CODES}.generic_error; a_error_string, a_error_message: like {C_STRING}.item)
			-- `a_error_code': SQLite3 return code
			-- `a_error_string': Generic message associated with `a_error_code'
			-- `a_error_message': Specific error message
		do
			make (a_error_code, create {STRING}.make_from_c (a_error_string), create {STRING}.make_from_c (a_error_message))
		ensure
			description: attached description
		end

feature -- Access

	tag: IMMUTABLE_STRING_32
			-- <Precursor>
		once
			result := {IMMUTABLE_STRING_32} "SQLite3 exception"
		end

end
