note
	description: "[
		SQLite3 statement that returns typed results.
		Beware, iteration is not without side effects. If several cursors need to iterate over the same statement, use `twin' or its variants.
	]"
	author: "obnosim"

class
	QUERY [ROW -> TUPLE [detachable ANY] create default_create end]

inherit

	STATEMENT
		rename
			apply as start,
			step as forth
		redefine
			start,
			forth,
			after
		end

	ITERABLE [ROW]
		rename
			twin as any_twin
		undefine
			out
		end

	DATA_TYPES
		rename
			twin as any_twin
		export {NONE} all
		undefine
			out
		end

create
	make, make_long_lived

feature -- Access

	new_cursor: QUERY_CURSOR [ROW]
			-- Cursor to iterate over all results of `Current'.
			-- Only one cursor per query should be used at the same time.
			-- If multiple concurrent cursors are needed, use `twin' or its variants or load up all data into a separate container at once.
		do
			create result.make (current)
			start
		end

	start
			-- <Precursor>
		do
			precursor
		ensure then
			success_or_off: success xor off
		end

	forth
			-- Iterates to the next row, if any
		do
			precursor
		ensure then
			success_or_off: success xor off
		end

	success: BOOLEAN
			-- Whether there are more results to be iterated over
		do
			result := last_return_code = more_rows
		end

	exhausted, off, after: BOOLEAN
			-- Whether there are no more results to be received from `Current'
		do
			result := last_return_code = rows_exhausted
		end

	max_column_count: INTEGER
			-- Total number of columns that can be in a single row returned by `Current'
		do
			result := ({ROW}).generic_parameter_count
		ensure
			instance_free: class
		end

	columns_in_row: INTEGER
			-- Number of columns in the current row of `Current'
		require
			has_row: success xor exhausted
		do
			result := sqlite3_data_count (internal_item)
		ensure
			bound_to_generics: result <= max_column_count
		end

	item: ROW
			-- Current row of returned data, mapped to a new `{C}'
		require
			has_data: success xor off
		do
			create result
			load_row (result)
		ensure then
			void_safe: across result.Lower |..| result.count is i all ({ROW}).generic_parameter_type (i).is_attached implies attached result [i] end
		end

	load_row (a_row: ROW)
			-- Copies and converts each column of the current row into the relevant item of `a_row'.
			-- Allows tuples to be reused.
			-- No column in `a_row' beyond the ones declared in `{C}' will be affected.
		require
			has_data: success xor off
		do
			map_row (a_row)
		ensure then
			void_safe: across {TUPLE}.Lower |..| columns_in_row is r all ({ROW}).generic_parameter_type (r).is_attached implies attached a_row [r] end
			supernumerary_columns_ignored: a_row.count > columns_in_row implies attached old a_row.twin as la_old and then across columns_in_row |..| a_row.count is i_supernumerary all a_row [i_supernumerary] = la_old [i_supernumerary] end
		end

feature -- Access

	first_or_default: detachable ROW
			-- The single result when called with `a_args', if any
		do
			start
			if success then
				result := item
			end
		end

	first: ROW
			-- The first result when called with `a_args'
		do
			check attached first_or_default as l_first then
				result := l_first
			end
		end

	single_or_default: detachable ROW
			-- The single result when called with `a_args', if any
		do
			result := first_or_default
			if success then
				forth
			end
		ensure
			end_of_results: exhausted
		end

	single, scalar: ROW
			-- The single result when called with `a_args'
		do
			check attached single_or_default as l_single then
				result := l_single
			end
		ensure
			end_of_results: exhausted
		end

	list: ARRAYED_LIST [ROW]
			-- All result rows when called with `a_args'
		do
			reset
			create result.make_from_iterable (current)
		ensure
			all_read: exhausted
		end

feature {NONE} -- Retrieval implementation

	map_row (a_row: ROW)
			-- Copies each column of the current row into the relevant item of `a_row'.
		require
			not_off: last_return_code = more_rows or last_return_code = rows_exhausted
		local
			i, l_end: like {TUPLE}.Lower
			l_source_type: like sqlite3_column_type
			l_target_type: TYPE [detachable ANY]
			l_converted: ANY
		do
			from
				i := {TUPLE}.Lower
				l_end := columns_in_row
			until
				i > l_end
			loop
				l_source_type := sqlite3_column_type (internal_item, i)
				l_target_type := a_row.generating_type.generic_parameter_type (i)
				if l_source_type = sqlite_null_type then
					check
						has_default: l_target_type.has_default
					then
						l_converted := l_target_type.default
					end
				elseif l_source_type = sqlite_integer_type then
					l_converted := from_int (sqlite3_column_int64 (internal_item, i), l_target_type)
				elseif l_source_type = sqlite_float_type then
					l_converted := from_float (sqlite3_column_double (internal_item, i), l_target_type)
				elseif l_source_type = sqlite_text_type then
					l_converted := from_text (sqlite3_column_text (internal_item, i), l_target_type)
				elseif l_source_type = sqlite_blob_type then
					l_converted := from_blob (i, l_target_type)
				else
					check
						unknown_type: False
					then
						do_nothing
					end
				end
				check
					correct_attachment: l_target_type.is_attached implies attached l_converted
					conforming: a_row.valid_type_for_index (l_converted, i)
				end
				a_row.put (l_converted, i)
				i := i + 1
			end
		ensure then
			void_safe: across {TUPLE}.Lower |..| columns_in_row is c all a_row.generating_type.generic_parameter_type (c).is_attached implies attached a_row [c] end
			supernumerary_columns_ignored: a_row.count > columns_in_row implies attached old a_row.twin as la_old and then across columns_in_row |..| a_row.count is i_supernumerary all a_row [i_supernumerary] = la_old [i_supernumerary] end
		end

	frozen from_int (a_source: INTEGER_64; a_output_type: TYPE [detachable ANY]): detachable ANY
			-- Applies the most suitable converter to produce instances of `a_output_type' from an sqlite int column, if any.
			-- Lookup is done in the following order: `Native_converters', `converters', `database.converters', `Global_converters'.
		do
			if attached {CONFIGURATION}.Native_converters.from_int [a_output_type] as la_native then
				result := la_native (a_source)
			elseif attached converters.from_int (a_output_type) as la_specific then
				result := la_specific (a_source)
			elseif attached database.converters.from_int (a_output_type) as la_per_db then
				result := la_per_db (a_source)
			elseif attached {CONFIGURATION}.Global_converters.from_int (a_output_type) as la_global then
				result := la_global (a_source)
			else
				check
					no_conversion: False
				then
					do_nothing
				end
			end
		ensure
			conforms_to_requested_type: attached result implies {ISE_RUNTIME}.type_conforms_to ({ISE_RUNTIME}.attached_type (result.generating_type.type_id), a_output_type.type_id)
			void_safe: not attached result implies a_output_type.has_default
		end

	frozen from_float (a_source: REAL_64; a_output_type: TYPE [detachable ANY]): detachable ANY
			-- Applies the most suitable converter to produce instances of `a_output_type' from an sqlite float column, if any.
			-- Lookup is done in the following order: `Native_converters', `converters', `database.converters', `Global_converters'.
		do
			if attached {CONFIGURATION}.Native_converters.from_float [a_output_type] as la_native then
				result := la_native (a_source)
			elseif attached converters.from_float (a_output_type) as la_specific then
				result := la_specific (a_source)
			elseif attached database.converters.from_float (a_output_type) as la_per_db then
				result := la_per_db (a_source)
			elseif attached {CONFIGURATION}.Global_converters.from_float (a_output_type) as la_global then
				result := la_global (a_source)
			else
				check
					no_conversion: False
				then
					do_nothing
				end
			end
		ensure
			conforms_to_requested_type: attached result implies {ISE_RUNTIME}.type_conforms_to ({ISE_RUNTIME}.attached_type (result.generating_type.type_id), a_output_type.type_id)
			void_safe: not attached result implies a_output_type.has_default
		end

	from_text (a_source: C_STRING; a_output_type: TYPE [detachable ANY]): detachable ANY
			-- Applies the most suitable converter to produce instances of `a_output_type' from an sqlite text column, if any.
			-- Lookup is done in the following order: `Native_converters', `converters', `database.converters', `Global_converters'.
		require
			valid_source: a_source.item /= default_pointer
		do
			if attached {CONFIGURATION}.Native_converters.from_text [a_output_type] as la_native then
				result := la_native (a_source)
			elseif attached converters.from_text (a_output_type) as la_specific then
				result := la_specific (a_source)
			elseif attached database.converters.from_text (a_output_type) as la_per_db then
				result := la_per_db (a_source)
			elseif attached {CONFIGURATION}.Global_converters.from_text (a_output_type) as la_global then
				result := la_global (a_source)
			else
				check
					no_conversion: False
				then
					do_nothing
				end
			end
		ensure
			conforms_to_requested_type: attached result implies {ISE_RUNTIME}.type_conforms_to ({ISE_RUNTIME}.attached_type (result.generating_type.type_id), a_output_type.type_id)
			void_safe: not attached result implies a_output_type.has_default
		end

	frozen from_blob (a_index: like max_column_count; a_output_type: TYPE [detachable ANY]): detachable ANY
			-- Column # `i' of the current row, treated as blob
		require
			valid: 1 <= a_index and a_index <= max_column_count
		do
			if attached sqlite3_column_blob (internal_item, a_index) as la_blob_data and then not la_blob_data.is_default_pointer then
				if {ISE_RUNTIME}.type_conforms_to (a_output_type.type_id, ({detachable BLOB [ANY]}).type_id) then
						-- Explicit BLOB mapping (through manifest type)
					check attached {BLOB [ANY]} {ISE_RUNTIME}.new_instance_of (a_output_type.type_id) as la_value then
						la_value.read (la_blob_data, sqlite3_column_bytes (internal_item, a_index))
						result := la_value
					end
				else
						-- Implicit BLOB mapping (through configuration)
					result := (create {BLOB [ANY]}.read (la_blob_data, sqlite3_column_bytes (internal_item, a_index))).item
				end
			end
		ensure
			conforms_to_requested_type: attached result implies {ISE_RUNTIME}.type_conforms_to ({ISE_RUNTIME}.attached_type (result.generating_type.type_id), a_output_type.type_id)
			void_safe: a_output_type.is_attached implies attached result
		end

feature {NONE} -- External

	frozen sqlite3_column_count (a_item: like internal_item): like max_column_count
			-- Expected number of columns returned by `Current'
		external
			"C (sqlite3_stmt*) : int | <sqlite3.h>"
		end

	frozen sqlite3_data_count (a_item: like internal_item): like max_column_count
			-- Actual number of columns in the current row
		external
			"C (sqlite3_stmt*) : int | <sqlite3.h>"
		end

	frozen sqlite3_column_type (a_item: like internal_item; a_index: like max_column_count): like sqlite_integer_type
			-- Type of the column # `a_index' in the current result row of `a_item'.
			-- Column indexes are corrected to start at 1.
		require
			valid: 1 <= a_index and a_index <= max_column_count
		external
			"C inline use <sqlite3.h>"
		alias
			"return sqlite3_column_type($a_item, $a_index - 1)"
		end

	frozen sqlite3_column_int (a_item: like internal_item; a_index: like max_column_count): INTEGER_32
			-- Int typed value of column # `a_index' of the current result row of `a_item'
			-- Column indexes are corrected to start at 1.
		require
			valid: 1 <= a_index and a_index <= max_column_count
		external
			"C inline use <sqlite3.h>"
		alias
			"return sqlite3_column_int($a_item, $a_index - 1)"
		end

	frozen sqlite3_column_int64 (a_item: like internal_item; a_index: like max_column_count): INTEGER_64
			-- Long typed value of column # `a_index' of the current result row of `a_item'
			-- Column indexes are corrected to start at 1.
		require
			valid: 1 <= a_index and a_index <= max_column_count
		external
			"C inline use <sqlite3.h>"
		alias
			"return sqlite3_column_int64($a_item, $a_index - 1)"
		end

	frozen sqlite3_column_double (a_item: like internal_item; a_index: like max_column_count): REAL_64
			-- Float typed value of column # `a_index' of the current result row of `a_item'
			-- Column indexes are corrected to start at 1.
		require
			valid: 1 <= a_index and a_index <= max_column_count
		external
			"C inline use <sqlite3.h>"
		alias
			"return sqlite3_column_double($a_item, $a_index - 1)"
		end

	frozen sqlite3_column_text (a_item: like internal_item; a_index: like max_column_count): C_STRING
			-- String typed value of column # `a_index' of the current result row of `a_item'
			-- Column indexes are corrected to start at 1.
		require
			valid: 1 <= a_index and a_index <= max_column_count
		do
			check attached c_sqlite3_column_text (a_item, a_index - 1) as la_value and then not la_value.is_default_pointer then
				create result.make_shared_from_pointer_and_count (la_value, sqlite3_column_bytes (internal_item, a_index))
				print("%NRestored c_string: " + result.string)
			end
		end

	frozen c_sqlite3_column_text (a_item: like internal_item; a_index: like max_column_count): like {detachable C_STRING}.item
			-- Implementation of `sqlite3_column_text', declared separately to avoid a spurrious external compilation warning
		require
			valid: 0 <= a_index and a_index < max_column_count
		external
			"C (sqlite3_stmt*, int) : const unsigned char* | <sqlite3.h>"
		alias
			"sqlite3_column_text"
		end

	frozen sqlite3_column_blob (a_item: like internal_item; a_index: like max_column_count): like {BLOB [ANY]}.internal_buffer_access
			-- Blob typed value of column # `a_index' of the current result row of `a_item'
			-- Column indexes are corrected to start at 1.
		require
			valid: 1 <= a_index and a_index <= max_column_count
		do
			result := c_sqlite3_column_blob (a_item, a_index - 1)
		end

	frozen c_sqlite3_column_blob (a_item: like internal_item; a_index: like max_column_count): like {BLOB [ANY]}.internal_buffer_access
			-- Implementation of `sqlite3_column_blob', declared separately to avoid a spurrious external compilation warning
		require
			valid: 0 <= a_index and a_index < max_column_count
		external
			"C (sqlite3_stmt*, int) : const void* | <sqlite3.h>"
		alias
			"sqlite3_column_blob"
		end

	frozen sqlite3_column_bytes (a_item: like internal_item; a_index: like max_column_count): INTEGER
			-- Size in bytes of the analogous `sqlite3_column_text' or `sqlite3_column_blob'
		require
			valid: 1 <= a_index and a_index <= max_column_count
		external
			"C inline use <sqlite3.h>"
		alias
			"return sqlite3_column_bytes($a_item, $a_index - 1)"
		end

end
