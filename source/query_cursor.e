note
	description: "Iterates over the results of a SQLite query"
	author: "obnosim"

class
	QUERY_CURSOR [ROW -> attached TUPLE [detachable ANY] create default_create end]

inherit

	ITERATION_CURSOR [ROW]

create {QUERY}
	make

feature {NONE} -- Initialization

	make (a_query: like query)
			-- `Current' shall iterate over the results of `a_query' one row at a time
		do
			query := a_query
			forth
		end

feature {NONE} -- Implementation

	query: QUERY [ROW]
			-- SQLite query providing the results over which to iterate

feature -- Access

	item: ROW
			-- <Precursor>
		attribute
			if {ISE_RUNTIME}.in_assertion then
				create result
			else
				check
					forth_not_called: false
				then
					do_nothing
				end
			end
		end

feature -- Status report

	after: BOOLEAN
			-- <Precursor>
		do
			result := query.after
		end

feature -- Cursor movement

	forth
			-- <Precursor>
		local
			l_row: like item
		do
			query.forth
			create l_row
			query.load_row (l_row)
			item := l_row
		ensure then
			item: attached item
			new_reference: item /= old item
		end

end
