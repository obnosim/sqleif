note
	description: "Sqlite extented error codes"
	author: "obnosim"

expanded class
	EXTENDED_ERROR_CODES

feature -- Extended error codes

	frozen error_missing_collseq: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_ERROR_MISSING_COLLSEQ"
		end

	frozen error_retry: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_ERROR_RETRY"
		end

	frozen error_snapshot: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_ERROR_SNAPSHOT"
		end

	frozen ioerr_read: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_READ"
		end

	frozen ioerr_short_read: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_SHORT_READ"
		end

	frozen ioerr_write: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_WRITE"
		end

	frozen ioerr_fsync: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_FSYNC"
		end

	frozen ioerr_dir_fsync: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_DIR_FSYNC"
		end

	frozen ioerr_truncate: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_TRUNCATE"
		end

	frozen ioerr_fstat: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_FSTAT"
		end

	frozen ioerr_unlock: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_UNLOCK"
		end

	frozen ioerr_rdlock: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_RDLOCK"
		end

	frozen ioerr_delete: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_DELETE"
		end

	frozen ioerr_blocked: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_BLOCKED"
		end

	frozen ioerr_nomem: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_NOMEM"
		end

	frozen ioerr_access: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_ACCESS"
		end

	frozen ioerr_checkreservedlock: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_CHECKRESERVEDLOCK"
		end

	frozen ioerr_lock: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_LOCK"
		end

	frozen ioerr_close: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_CLOSE"
		end

	frozen ioerr_dir_close: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_DIR_CLOSE"
		end

	frozen ioerr_shmopen: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_SHMOPEN"
		end

	frozen ioerr_shmsize: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_SHMSIZE"
		end

	frozen ioerr_shmlock: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_SHMLOCK"
		end

	frozen ioerr_shmmap: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_SHMMAP"
		end

	frozen ioerr_seek: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_SEEK"
		end

	frozen ioerr_delete_noent: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_DELETE_NOENT"
		end

	frozen ioerr_mmap: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_MMAP"
		end

	frozen ioerr_gettemppath: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_GETTEMPPATH"
		end

	frozen ioerr_convpath: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_CONVPATH"
		end

	frozen ioerr_vnode: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_VNODE"
		end

	frozen ioerr_auth: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_AUTH"
		end

	frozen ioerr_begin_atomic: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_BEGIN_ATOMIC"
		end

	frozen ioerr_commit_atomic: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_COMMIT_ATOMIC"
		end

	frozen ioerr_rollback_atomic: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR_ROLLBACK_ATOMIC"
		end

	frozen locked_sharedcache: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_LOCKED_SHAREDCACHE"
		end

	frozen locked_vtab: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_LOCKED_VTAB"
		end

	frozen busy_recovery: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_BUSY_RECOVERY"
		end

	frozen busy_snapshot: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_BUSY_SNAPSHOT"
		end

	frozen cantopen_notempdir: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CANTOPEN_NOTEMPDIR"
		end

	frozen cantopen_isdir: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CANTOPEN_ISDIR"
		end

	frozen cantopen_fullpath: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CANTOPEN_FULLPATH"
		end

	frozen cantopen_convpath: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CANTOPEN_CONVPATH"
		end

	frozen cantopen_dirtywal: INTEGER
			-- Not used
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CANTOPEN_DIRTYWAL"
		end

	frozen corrupt_vtab: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CORRUPT_VTAB"
		end

	frozen corrupt_sequence: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CORRUPT_SEQUENCE"
		end

	frozen readonly_recovery: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_READONLY_RECOVERY"
		end

	frozen readonly_cantlock: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_READONLY_CANTLOCK"
		end

	frozen readonly_rollback: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_READONLY_ROLLBACK"
		end

	frozen readonly_dbmoved: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_READONLY_DBMOVED"
		end

	frozen readonly_cantinit: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_READONLY_CANTINIT"
		end

	frozen readonly_directory: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_READONLY_DIRECTORY"
		end

	frozen abort_rollback: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_ABORT_ROLLBACK"
		end

	frozen constraint_check: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CONSTRAINT_CHECK"
		end

	frozen constraint_commithook: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CONSTRAINT_COMMITHOOK"
		end

	frozen constraint_foreignkey: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CONSTRAINT_FOREIGNKEY"
		end

	frozen constraint_function: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CONSTRAINT_FUNCTION"
		end

	frozen constraint_notnull: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CONSTRAINT_NOTNULL"
		end

	frozen constraint_primarykey: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CONSTRAINT_PRIMARYKEY"
		end

	frozen constraint_trigger: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CONSTRAINT_TRIGGER"
		end

	frozen constraint_unique: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CONSTRAINT_UNIQUE"
		end

	frozen constraint_vtab: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CONSTRAINT_VTAB"
		end

	frozen constraint_rowid: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CONSTRAINT_ROWID"
		end

	frozen notice_recover_wal: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_NOTICE_RECOVER_WAL"
		end

	frozen notice_recover_rollback: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_NOTICE_RECOVER_ROLLBACK"
		end

	frozen warning_autoindex: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_WARNING_AUTOINDEX"
		end

	frozen auth_user: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_AUTH_USER"
		end

	frozen ok_load_permanently: INTEGER
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_OK_LOAD_PERMANENTLY"
		end

end
