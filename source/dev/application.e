note
	description: "Application root"
	author: "obnosim"

class
	APPLICATION

create
	make

feature {NONE} -- Initialization

	make
			-- Entrypoint for testing the library
		local
			l_db: DATABASE
			l_start: TIME
		do
			create l_start.make_now
			print ("%N----%N")
			create l_db.force_create_open_write ("db/test.db")
			create_test_db (l_db)
			across
				l_db.query ("[
					select
						boolean,                boolean_ref,
						integer,                integer_ref,
						natural_64,             natural_64_ref,
						float,                  float_ref,
						double,                 double_ref,
						attached_text_8,        detachable_text_8,
						attached_text_32,       detachable_text_32,
						attached_blob,          detachable_blob,
						attached_implicit_blob, detachable_implicit_blob
					from TEST
				]", void, test_table_row) as row
			loop
				print ("%N----")
				across
					1 |..| test_table_row.count as r
				loop
					print ("%N" + r.item.out + " " + test_table_column_names [r.item] + ": ")
					print_cell (row.item [r.item])
				end
			end
			print ("%N")
			print (((create {TIME}.make_now) - l_start).duration)
		end

	print_cell (a_value: detachable ANY)
		local
			i: INTEGER
			l_value: ANY
		do
			l_value := a_value
			if attached {BLOB [ANY]} l_value as la_blob then
				print (l_value.generating_type)
				print (' ')
				l_value := la_blob.item
			end
			if not attached l_value as la_value then
				print ("(null)")
			elseif attached {READABLE_STRING_GENERAL} l_value then
				print (l_value)
			elseif attached {CONTAINER [detachable ANY]} l_value as la_container then
				print (la_container.generating_type)
				across
					la_container as c
				from
					i := 1
				loop
					print ("%N%T")
					print (i)
					print (" : ")
					if attached c.item as la_item then
						print ('{')
						print (la_item.generating_type.name)
						print ("} ")
						print (c.item)
					else
						print ("(null)")
					end
					i := i + 1
				end
			else
				print (la_value)
			end
		end

	test_table_row: TUPLE [ --
			boolean: BOOLEAN; boolean_ref: detachable BOOLEAN_REF; --
			 integer: INTEGER; integer_ref: detachable INTEGER_REF; --
			 natural_64: NATURAL_64; natural_64_ref: detachable NATURAL_64_REF; --
			 float: REAL_32; float_ref: detachable REAL_REF; --
			 double: REAL_64; double_ref: detachable DOUBLE_REF; --
			 attached_text_8: STRING; detachable_text_8: detachable STRING_8; --
			 attached_text_32: STRING_32; detachable_text_32: detachable STRING_32; --
			 attached_blob: BLOB [TABLE [ANY, ANY]]; detachable_blob: detachable BLOB [TABLE [ANY, ANY]]; --
			 attached_implicit_blob: TABLE [ANY, ANY]; detachable_implicit_blob: detachable TABLE [ANY, ANY] --
		]
		do
			create result
		end

	test_table_column_names: ARRAY [READABLE_STRING_8]
		once ("object")
			result := <<"boolean", "boolean_ref", --
						 "integer", "integer_ref", --
						 "natural_64", "natural_64_ref", --
						 "float", "float_ref", --
						 "double", "double_ref", --
						 "attached_text_8", "detachable_text_8", --
						 "attached_text_32", "detachable_text_32", --
						 "attached_blob", "detachable_blob", --
						 "attached_implicit_blob", "detachable_implicit_blob">>
		end

	create_test_db (a_database: DATABASE)
		local
			l_insert: INSERT_STATEMENT [like test_table_row]
			i: INTEGER
			l_row: like test_table_row
		do
			a_database.execute ("[
				create table TEST (
					boolean                  int not null, boolean_ref                int null,
					integer                  int not null, integer_ref              int64 null,
					natural_64             int64 not null, natural_64_ref           int64 null,
					float                  float not_null, float_ref                float null,
					double                 float not_null, double_ref               float null,
					attached_text_8         text not null, detachable_text_8         text null,
					attached_text_32        text not null, detachable_text_32        text null,
					attached_blob           blob not null, detachable_blob           blob null,
					attached_implicit_blob  blob not null, detachable_implicit_blob  blob null
				);
			]");
				-- Use implicit blob
			create l_insert.make_long_lived (a_database, "TEST", test_table_column_names)
			l_insert.configure ({TO_SPECIAL [INTEGER]}).store_as_blob
			across
				1 |..| 3 as range
			loop
				i := range.item
				create l_row
				l_row.boolean := i \\ 2 = 1
				l_row.integer := - i
				if i \\ 2 /= 0 then
					l_row.boolean_ref := (i \\ 2 = 1).to_reference
					l_row.integer_ref := i.to_reference
					l_row.natural_64_ref := (i.as_natural_64 + {INTEGER_32}.Max_value.as_natural_64).to_reference
					l_row.float_ref := (i * {MATH_CONST}.Pi.truncated_to_real).to_reference
					l_row.double_ref := (i * {MATH_CONST}.Pi).to_reference
					l_row.detachable_text_8 := if i = 1 then "test �" else "test �" end
					l_row.detachable_text_32 := if i = 1 then {STRING_32} "test �" else {STRING_32} "test �" end
					l_row.detachable_blob := create {BLOB [TABLE [INTEGER, INTEGER]]}.write (create {ARRAY [INTEGER]}.make_filled (i, 1, i))
					l_row.detachable_implicit_blob := create {ARRAY [INTEGER]}.make_filled (i, 1, i)
				end
				l_row.natural_64 := i.as_natural_64 + {INTEGER_32}.Max_value.as_natural_64
				l_row.float := i * {MATH_CONST}.Pi.truncated_to_real
				l_row.double := i * {MATH_CONST}.Pi
				l_row.attached_text_8 := if i = 1 then "test �" elseif i = 2 then "test �" else "test �" end
				l_row.attached_text_32 := if i = 1 then {STRING_32} "test �" elseif i = 2 then {STRING_32} "test �" else {STRING_32} "test �" end
				l_row.attached_blob := create {BLOB [TABLE [INTEGER, INTEGER]]}.write (create {ARRAY [INTEGER]}.make_filled (i, 1, i))
				l_row.attached_implicit_blob := create {ARRAY [INTEGER]}.make_filled (i, 1, i)
				l_insert.call (l_row)
			end
		end

end
