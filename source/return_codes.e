note
	description: "[
		Constants used by SQLite3 as return and error codes.
		Documentation taken verbatim from <sqlite3.h>
	]"
	author: "obnosim"

expanded class
	RETURN_CODES

feature {NONE} -- Return codes

	frozen success_code: INTEGER
			-- Successful result
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_OK"
		end

	frozen generic_error: INTEGER
			-- Generic error
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_ERROR"
		end

	frozen internal_error: INTEGER
			-- Internal logic error in SQLite
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_INTERNAL"
		end

	frozen permission_denied: INTEGER
			-- Access permission denied
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_PERM"
		end

	frozen abort_requested: INTEGER
			-- Callback routine requested an abort
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_ABORT"
		end

	frozen database_busy: INTEGER
			-- The database file is locked
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_BUSY"
		end

	frozen database_locked: INTEGER
			-- A table in the database is locked
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_LOCKED"
		end

	frozen no_memory: INTEGER
			-- A malloc() failed
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_NOMEM"
		end

	frozen write_on_readonly: INTEGER
			-- Attempt to write a readonly database
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_READONLY"
		end

	frozen operation_interrupted: INTEGER
			-- Operation terminated by sqlite3_interrupt()
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_INTERRUPT"
		end

	frozen io_error: INTEGER
			-- Some kind of disk I/O error occurred
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_IOERR"
		end

	frozen database_corrupt: INTEGER
			-- The database disk image is malformed
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CORRUPT"
		end

	frozen file_not_found: INTEGER
			-- Unknown opcode in sqlite3_file_control()
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_NOTFOUND"
		end

	frozen database_full: INTEGER
			-- Insertion failed because database is full
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_FULL"
		end

	frozen cannot_open_database: INTEGER
			-- Unable to open the database file
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CANTOPEN"
		end

	frozen protocol_error: INTEGER
			-- Database lock protocol error
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_PROTOCOL"
		end

	frozen schema_changed: INTEGER
			-- The database schema changed
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_SCHEMA"
		end

	frozen constraint_violation: INTEGER
			-- Abort due to constraint violation
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_CONSTRAINT"
		end

	frozen incorrect_use: INTEGER
			-- Library used incorrectly
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_MISUSE"
		end

	frozen not_supported: INTEGER
			-- Uses OS features not supported on host
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_NOLFS"
		end

	frozen authorization_denied: INTEGER
			-- Authorization denied
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_AUTH"
		end

	frozen not_a_database: INTEGER
			-- File opened that is not a database file
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_NOTADB"
		end

feature {NONE} -- sqlite3_log return codes

	frozen notifications: INTEGER
			-- Notifications from sqlite3_log()
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_NOTICE"
		end

	frozen warnings: INTEGER
			-- Warnings from sqlite3_log()
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_WARNING"
		end

feature {NONE} -- sqlite_bind_* error codes

	frozen string_or_blob_too_large: INTEGER
			-- String or BLOB exceeds size limit
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_TOOBIG"
		end

	frozen type_mismatch: INTEGER
			-- Data type mismatch
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_MISMATCH"
		end

	frozen invalid_operand_index: INTEGER
			-- 2nd parameter to sqlite3_bind out of range
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_RANGE"
		end

feature {NONE} -- sqlite3_step

	frozen more_rows: INTEGER
			-- sqlite3_step() has another row ready
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_ROW"
		end

	frozen rows_exhausted: INTEGER
			-- sqlite3_step() has finished executing
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_DONE"
		end

end
