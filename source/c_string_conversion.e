note
	description: "Common utilities to interact with C_STRINGs"
	author: "obnosim"

class
	C_STRING_CONVERSION

feature -- Eiffel to C

	frozen readable_string_8_to_c (a_string: READABLE_STRING_8): C_STRING
			-- Converts `a_string' into a C string
		local
			u_utf: UTF_CONVERTER
			l_utf_8: STRING_8
		do
			if u_utf.is_valid_utf_8_string_8 (a_string) then
				l_utf_8 := a_string.to_string_8
			else
				l_utf_8 := u_utf.string_32_to_utf_8_string_8 (a_string.as_string_32)
			end
			create result.make (l_utf_8)
		ensure
			utf8: {UTF_CONVERTER}.is_valid_utf_8_string_8 (result.string)
			instance_free: class
		end

	frozen readable_string_32_to_c (a_string: READABLE_STRING_32): C_STRING
			-- Converts `a_string' into a C string
		do
			create result.make ({UTF_CONVERTER}.string_32_to_utf_8_string_8 (a_string))
		ensure
			utf8: {UTF_CONVERTER}.is_valid_utf_8_string_8 (result.string)
			instance_free: class
		end

	frozen readable_string_general_to_c (a_string: READABLE_STRING_GENERAL): C_STRING
			-- Convenience to invoke either `string_8_to_c' or `string_32_to_c' depending on the type of `a_string'
		require
			string_8_or_32: attached {READABLE_STRING_8} a_string xor attached {READABLE_STRING_32} a_string
		do
			if a_string.is_string_8 then
				result := string_8_to_c (a_string.to_string_8)
			else
				result := string_32_to_c (a_string.to_string_32)
			end
		ensure
			utf8: {UTF_CONVERTER}.is_valid_utf_8_string_8 (result.string)
			instance_free: class
		end

	frozen string_8_to_c (a_string: STRING_8): C_STRING
			-- Converts `a_string' into a C string
		do
			result := readable_string_8_to_c (a_string)
		ensure
			utf8: {UTF_CONVERTER}.is_valid_utf_8_string_8 (result.string)
			instance_free: class
		end

	frozen string_32_to_c (a_string: STRING_32): C_STRING
			-- Converts `a_string' into a C string
		do
			result := readable_string_32_to_c (a_string)
		ensure
			utf8: {UTF_CONVERTER}.is_valid_utf_8_string_8 (result.string)
			instance_free: class
		end

	frozen string_general_to_c (a_string: STRING_GENERAL): C_STRING
			-- Convenience to invoke either `string_8_to_c' or `string_32_to_c' depending on the type of `a_string'
		require
			string_8_or_32: attached {READABLE_STRING_8} a_string xor attached {READABLE_STRING_32} a_string
		do
			result := readable_string_general_to_c (a_string)
		ensure
			utf8: {UTF_CONVERTER}.is_valid_utf_8_string_8 (result.string)
			instance_free: class
		end

	frozen immutable_string_8_to_c (a_string: IMMUTABLE_STRING_8): C_STRING
			-- Converts `a_string' into a C string
		do
			result := readable_string_8_to_c (a_string)
		ensure
			utf8: {UTF_CONVERTER}.is_valid_utf_8_string_8 (result.string)
			instance_free: class
		end

	frozen immutable_string_32_to_c (a_string: IMMUTABLE_STRING_32): C_STRING
			-- Converts `a_string' into a C string
		do
			result := readable_string_32_to_c (a_string)
		ensure
			utf8: {UTF_CONVERTER}.is_valid_utf_8_string_8 (result.string)
			instance_free: class
		end

	frozen immutable_string_general_to_c (a_string: IMMUTABLE_STRING_GENERAL): C_STRING
			-- Convenience to invoke either `string_8_to_c' or `string_32_to_c' depending on the type of `a_string'
		require
			string_8_or_32: attached {READABLE_STRING_8} a_string xor attached {READABLE_STRING_32} a_string
		do
			result := readable_string_general_to_c (a_string)
		ensure
			utf8: {UTF_CONVERTER}.is_valid_utf_8_string_8 (result.string)
			instance_free: class
		end

feature -- C to Eiffel

	frozen c_to_string_8 (a_string: C_STRING): STRING_8
			-- Converts `a_string' into an Eiffel string
		do
			result := a_string.string
		ensure
			valid: {UTF_CONVERTER}.is_valid_utf_8_string_8 (result)
			instance_free: class
		end

	frozen c_to_immutable_string (a_string: C_STRING): IMMUTABLE_STRING_8
			-- Converts `a_string' into an immutable Eiffel string
		do
			result := c_to_string_8 (a_string)
		ensure
			valid: {UTF_CONVERTER}.is_valid_utf_8_string_8 (result)
			instance_free: class
		end

	frozen c_to_string_32 (a_string: C_STRING): STRING_32
			-- Converts `a_string' into an Eiffel string
		do
			result := {UTF_CONVERTER}.utf_8_string_8_to_string_32 (a_string.string)
		ensure
			instance_free: class
		end

	frozen c_to_immutable_string_32 (a_string: C_STRING): IMMUTABLE_STRING_32
			-- Converts `a_string' into an immutable Eiffel string
		do
			result := c_to_string_32 (a_string)
		ensure
			instance_free: class
		end

end
