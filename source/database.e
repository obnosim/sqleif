note
	description: "Wrapper for SQLite3 databases"
	author: "obnosim"

class
	DATABASE

inherit

	C_STRING_CONVERSION
		export
			{NONE} all
		end

	CONFIGURABLE
		rename
			store as converters
		end

	DISPOSABLE
		export {NONE} all end

	DEBUG_OUTPUT
		rename
			debug_output as path
		end

create
	open_read, open_write, create_open_write, force_create_open_write

convert
	internal_item: {POINTER}

feature {NONE} -- Initialization

	open (a_path: READABLE_STRING_32; a_flags: like sqlite_open_readonly)
			-- Opens the database file at `a_path' with access rights according to `a_flags'
		do
			path := a_path
			handle_error (sqlite3_open_v2 (string_32_to_c (path).item, $internal_item, a_flags, create {POINTER}))
			enable_extended_result_codes
			create converters
		end

	open_write (a_path: READABLE_STRING_32)
			-- Opens the database file at `a_path' with read and write access
		do
			open (a_path, sqlite_open_create_readwrite)
		end

	open_read (a_path: READABLE_STRING_32)
			-- Opens the database file at `a_path' with read-only access
		do
			open (a_path, sqlite_open_readonly)
		end

	create_open_write (a_path: READABLE_STRING_32)
			-- Opens the database file at `a_path' with read and write access.
			-- If it does not exists, it will be created.
		local
			l_file: PLAIN_TEXT_FILE
			l_directory: DIRECTORY
		do
			debug ("sqleif_force_create_db")
				delete_existing_file (a_path)
			end
			create l_file.make_with_name (a_path)
			if not l_file.exists then
				create l_directory.make_with_path (l_file.path.parent)
				if not l_directory.exists then
					l_directory.recursive_create_dir
				end
				open_write (a_path)
				after_create
			else
				open_write (a_path)
			end
		end

	force_create_open_write (a_path: READABLE_STRING_32)
			-- Creates the database file at `a_path' with read and write access.
			-- If it already not existed, it will be deleted beforehand.
		do
			delete_existing_file (a_path)
			create_open_write (a_path)
		end

	delete_existing_file (a_path: READABLE_STRING_32)
			-- Deletes the file `a_path' if it exists
		local
			l_file: PLAIN_TEXT_FILE
		do
			create l_file.make_with_name (a_path)
			if l_file.exists then
				l_file.delete
			end
		end

	after_create
			-- Hook called after the file for `Current' is first created
		do
			do_nothing
		end

	frozen sqlite3_open_v2 (a_filename: like {C_STRING}.item; a_output: TYPED_POINTER [like internal_item]; a_flags: INTEGER; a_vfs: detachable like {C_STRING}.item): INTEGER
			-- Pointer to a newly opened SQLite database object targetting `a_filename' with flags `a_flags'
		external
			"C (const char*, sqlite3**, int, const char*) : int | <sqlite3.h>"
		end

	frozen sqlite_open_readonly: INTEGER
			-- Flag indicating that a database is to be opened with read-only access
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_OPEN_READONLY"
		end

	frozen sqlite_open_create_readwrite: INTEGER
			-- Flag indicating that a database is to be created if needed and opened with read-write access
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE"
		end

	frozen sqlite_open_readwrite: INTEGER
			-- Flag indicating that a database is to be opened with read-write access
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_OPEN_READWRITE"
		end

	frozen sqlite_open_create: INTEGER
			-- Flag indicating that a database is to be created if it does not exist
			-- To be used in combination with `c_sqlite_flag_open_readwrite'
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_OPEN_CREATE"
		end

feature {STATEMENT} -- Implementation

	path: READABLE_STRING_32
			-- Path of the backing file

	internal_item: POINTER
			-- Pointer to SQLite database object
feature -- Access

	execute alias "()" (a_command: READABLE_STRING_8; a_args: detachable TUPLE)
			-- Executes the command `a_command' on `Current'
		do
			if attached a_args and then not a_args.is_empty then
				(create {PARAMETERIZED_STATEMENT [like a_args]}.make (current, a_command)).call (a_args)
			else
				(create {STATEMENT}.make (current, a_command)).apply
			end
		end

	query (a_statement: READABLE_STRING_8; a_args: detachable TUPLE [detachable ANY]; a_result_container: attached TUPLE [detachable ANY]): QUERY [like a_result_container]
			-- A new SQLite query on `Current' executing `a_statement' and reading rows into `a_result_container' using `a_args' as bound operands.
			-- `a_operands' must be `void' if `a_statement' requires none.
			-- The result is optimized for few uses.
		local
			l_parameterized: PARAMETERIZED_QUERY [attached like a_args, like a_result_container]
		do
			if attached a_args then
				create l_parameterized.make (current, a_statement)
				l_parameterized.bind (a_args)
				result := l_parameterized
			else
				create result.make (current, a_statement)
			end
		end

	long_lived_query (a_statement: READABLE_STRING_8; a_args: detachable TUPLE [detachable ANY]; a_result_container: attached TUPLE [detachable ANY]): QUERY [like a_result_container]
			-- A new SQLite query on `Current' executing `a_statement' and reading rows into `a_result_container' using `a_args' as bound operands.
			-- `a_operands' must be `void' if `a_statement' requires none.
			-- The result is optimized for several uses.
		local
			l_parameterized: PARAMETERIZED_QUERY [attached like a_args, like a_result_container]
		do
			if attached a_args then
				create l_parameterized.make_long_lived (current, a_statement)
				l_parameterized.bind (a_args)
				result := l_parameterized
			else
				create result.make_long_lived (current, a_statement)
			end
		end

feature -- Tables

	has_table (a_name: READABLE_STRING_8): BOOLEAN
			-- Whether `Current' has a table named `a_name'
		local
			l_query: QUERY [TUPLE [matches: INTEGER]]
		do
			l_query := query ("select count(*) from sqlite_master where type = 'table' and name = ?;", [a_name], create {TUPLE [INTEGER]})
			l_query.start
			result := l_query.item.matches > 0
		end

	drop_table (a_name: READABLE_STRING_8)
			-- Deletes the table named `a_name'
		require
			table_exists: has_table (a_name)
		local
			l_command: STATEMENT
			l_attempts: INTEGER_8
		do
			check
				max_attempts: l_attempts < 3
			then
				create l_command.make (current, "drop table " + a_name + ";")
				l_command.apply
			end
		ensure
			dropped: not has_table (a_name)
		rescue
			l_attempts := l_attempts + 1
			retry
		end

feature -- Transactions

	begin_transaction
			-- Starts a transaction.
			-- Must be called in conjunction with either `commit_transaction' or `rollback_transaction'.
		do
			execute (once "begin transaction;")
		end

	commit_transaction
			-- Commits the transaction previously started by `begin_transaction'
		do
			execute (once "commit transaction;")
		end

	rollback_transaction
			-- Cancels the transaction previously started by `begin_transaction'
		do
			execute (once "rollback transaction;")
		end

feature -- Configuration

	converters: TYPE_CONVERTERS
			-- Custom converters, specific to `Current'.
			-- Takes precedence over `Global_converters'.
feature {STATEMENT} -- Error handling

	handle_last_error
			-- s a debug message for the last SQLite error code returned by `Current', then raises and assertion exception.
			-- For debugging purposes only.
		do
			handle_error (sqlite3_extended_errcode (internal_item))
		end

	handle_error (a_code: like sqlite3_extended_errcode)
			-- s a debug message for the SQLite error code `a_code', then raises and assertion exception.
			-- For debugging purposes only.
		do
			if a_code /= 0 then
				(create {DATABASE_EXCEPTION}.make_from_c (a_code, sqlite3_errstr (a_code), sqlite3_errmsg (internal_item))).raise
			end
		end

feature {NONE} -- Error handling

	frozen sqlite3_extended_result_codes (a_item: like internal_item; a_state: INTEGER): INTEGER
			-- Enables or disables extended result codes when using `a_item' 'according to `a_state'
		external
			"C (sqlite3*, int) : int | <sqlite3.h>"
		end

	frozen enable_extended_result_codes
			-- Enables extended result codes for more granular debugging
		do
			sqlite3_extended_result_codes (internal_item, (true).to_integer).do_nothing
		end

	frozen disable_extended_result_codes
			-- Disables extended result codes
		do
			sqlite3_extended_result_codes (internal_item, (false).to_integer).do_nothing
		end

	frozen sqlite3_extended_errcode (a_item: like internal_item): INTEGER
			-- Code of the last error from `a_item'
		external
			"C (sqlite3*) : const char* | <sqlite3.h>"
		end

	frozen sqlite3_errmsg (a_item: like internal_item): like {C_STRING}.item
			-- Message of the last error from `a_item'
		external
			"C (sqlite3*) : const char* | <sqlite3.h>"
		end

	frozen sqlite3_errstr (a_code: like sqlite3_extended_errcode): like sqlite3_errmsg
			-- Message associated to `a_code'
		external
			"C (int) : const char* | <sqlite3.h>"
		end

feature {NONE} -- Disposal

	dispose
			-- Frees `internal_item'
		do
			debug ("disposal")
				print ("%N" + generating_type.name + "::Dispose")
			end
			sqlite3_close_v2 (internal_item).do_nothing
		end

	frozen sqlite3_close_v2 (a_item: POINTER): INTEGER
			-- Finalizes `a_item'
		external
			"C (sqlite3*) : int | <sqlite3.h>"
		end

invariant
	internal_item: internal_item /= default_pointer

end
