note
	description: "[
		External constants identifying the fundamental types in SQLite3
		and helpful methods to handle them.
	]"
	author: "obnosim"

class
	DATA_TYPES

feature {NONE} -- Column types

	frozen sqlite_null_type: INTEGER
			-- Null column type code
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_NULL"
		end

	frozen sqlite_integer_type: INTEGER
			-- Integer column type code
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_INTEGER"
		end

	frozen sqlite_float_type: INTEGER
			-- Float column type code
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_FLOAT"
		end

	frozen sqlite_text_type: INTEGER
			-- String column type code
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE3_TEXT"
		end

	frozen sqlite_blob_type: INTEGER
			-- Blob column type code
		external
			"C inline use <sqlite3.h>"
		alias
			"SQLITE_BLOB"
		end

feature {NONE} -- Debug

	type_name (a_type: like sqlite_integer_type): IMMUTABLE_STRING_8
			-- Explicit name of sqlite type `a_type', for debugging purposes
		do
			if a_type = sqlite_integer_type then
				result := once "int"
			elseif a_type = sqlite_float_type then
				result := once "float"
			elseif a_type = sqlite_text_type then
				result := once "text"
			elseif a_type = sqlite_blob_type then
				result := once "blob"
			elseif a_type = sqlite_null_type then
				result := once "null"
			else
				check
					invalid_type: False
				then
					do_nothing
				end
			end
		ensure
			instance_free: class
		end

	compatible (a_result_tuple: TUPLE; a_position: like {TUPLE}.Lower; a_expected, a_actual: like sqlite_integer_type): BOOLEAN
			-- Whether the types `a_expected' and `a_actual' match.
			-- If not, s a detailed warning using `a_result_type' and `a_position' to hint at the error.
			-- To be used as an assertion.
		do
			if a_expected /= a_actual then
				print ("%NInvalid result for column " + a_position.out + " of " + a_result_tuple.generating_type.name + ": expected " + type_name (a_expected) + " but got " + type_name (a_actual))
			else
				result := true
			end
		ensure
			instance_free: class
		end

end
