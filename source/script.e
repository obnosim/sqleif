note
	description: "Multiline consecutive statements, which can either be executed all at once or step by step"
	author: "obnosim"

class
	SCRIPT

inherit

	STATEMENT
		rename
			apply as run_till_end,
			statement as script
		redefine
			c_prepare_statement, reset, step, run_till_end, after
		end

create
	make, make_long_lived

feature {NONE} -- Initialization

	c_prepare_statement (a_c_statement: like {C_STRING}.item; a_c_statement_length: like {C_STRING}.count; a_flags: NATURAL): INTEGER
			-- <Precursor>
		local
			l_old_tail: like next_statement_position
		do
			l_old_tail := next_statement_position
			result := sqlite3_prepare_v3 (database, a_c_statement, a_c_statement_length, a_flags, $internal_item, $next_statement_position)
			if next_statement_position.to_integer_32 <= l_old_tail.to_integer_32 then
				next_statement_position := default_pointer
			end
		ensure then
			instanciated: internal_item.is_default_pointer = after
		end

feature {NONE} -- Implementation

	next_statement_position: like {C_STRING}.item
			-- Offset within `script', indicating the beginning of the next statement to be executed
		attribute
		ensure
			result.is_default_pointer implies internal_item.is_default_pointer
		end

feature -- Access

	reset
			-- <Precursor>
		do
			sqlite3_finalize (internal_item).do_nothing
			next_statement_position := default_pointer
			if long_lived then
				make_long_lived (database, script)
			else
				make (database, script)
			end
		end

	step
			-- Executes the next statement within the script
		do
			if not after then
				last_return_code := sqlite3_step (internal_item)
				handle_error (last_return_code)
				sqlite3_finalize (internal_item).do_nothing
				handle_error (c_prepare_statement (next_statement_position, -1, if long_lived then sqlite_prepare_flag_persistent else {NATURAL} 0 end))
			end
		end

	after: BOOLEAN
			-- <Precursor>
		do
			result := next_statement_position = default_pointer
		end

	run_till_end
			-- Executes from beginning to end
		do
			from
				step
			until
				after
			loop
				step
			end
		ensure then
			finished: after
		end

end
