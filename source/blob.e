note
	description: "[
		Converts objects to binary blob and vice-versa.
		
		G may be any attached type except POINTER_REF and its descendants, with the following caveats:
			- A retrieved item may differ (as per 'is_deep_equal') from the original if it has 'once ("object")' attributes,
				but those should work correctly following retrieval nevertheless.
			- Fields of type POINTER_REF or descendants are lost during conversion, so this should be accounted for.
			- As items are restored reflectively, no creation procedure will be called.
	]"
	author: "obnosim"

frozen class
	BLOB [G -> attached ANY]

inherit

	STREAM
		rename
			item as pointer,
			object_stored_size as bytes_count
		export
			{NONE} all
			{ANY} deep_copy, deep_twin, is_deep_equal, standard_is_equal, default_create, is_closed, generating_type, default
			{QUERY, PARAMETERIZED_STATEMENT} pointer, bytes_count
		redefine
			out
		end

create
	write, read

convert
	write ({G}),
	item: {G}

feature {NONE} -- Initialization

	write (a_item: G)
			-- `Current' shall hold `a_item' in blob form
		do
			make
			item := a_item
			independent_store (a_item)
		end

feature {BLOB, QUERY} -- Initialization

	read (a_data: POINTER; a_size: INTEGER)
			-- `Current' shall retrieve values in blob format from the `a_size' first bytes of `a_buffer'
		require
			valid_pointer: not a_data.is_default_pointer
			valid_size: a_size > 0
		do
			make_with_size (a_size)
			bytes_count := a_size
			pointer.memory_copy (a_data, a_size)
			read_item
		ensure
			set: attached item
			closed: is_closed
		end

feature -- Access

	item: G
			-- Eiffel object stored in `Current'
		attribute
		end

feature {NONE} -- Implementation

	read_item
			-- Retrieves the serialized object into `item'
		require
			open: not is_closed
		do
			if attached retrieved as la_any then
				debug ("sqleif")
					if not attached {G} la_any then
						print ("%NRetrieved blob value does not match expected type: expected " + ({G}).name + " but got " + la_any.generating_type.name)
					end
				end
				check
					matching_type: attached {G} la_any as la_g
				then
					item := la_g
				end
			else
				check
					must_allow_void: ({G}).has_default
				then
					item := ({G}).default
				end
			end
			close
		ensure
			set: attached item
			closed: is_closed
		end

feature -- Debug

	out: STRING
			-- <Precursor>
		do
			if attached item then
				result := "(blob) = '" + item.out + "'"
			else
				result := "(uninitialized blob)"
			end
		end

invariant
	persistable: not {ISE_RUNTIME}.type_conforms_to (({G}).type_id, ({detachable POINTER_REF}).type_id)

end
