note
	description: "Useful features to work with routines, procedures and functions"
	author: "obnosim"

class
	TUPLE_UTIL

feature -- Routines

	frozen input_type (a_routine: ROUTINE [detachable ANY]): TYPE [detachable ANY]
			-- Runtime declared input type of `a_function'.
			-- Only meaningful if `a_routine' takes only one operand.
		require
			valid: a_routine.open_count = 1
		do
			result := {REFLECTOR}.type_of_type ({ISE_RUNTIME}.dynamic_type (a_routine)).generic_parameter_type (1).generic_parameter_type (1)
		ensure
			instance_free: class
		end

	frozen input_type_id (a_routine: ROUTINE [detachable ANY]): like {TYPE [detachable ANY]}.type_id
			-- Type id of the runtime declared input type of `a_function'.
			-- Only meaningful if `a_routine' takes only one operand.
		require
			valid: a_routine.open_count = 1
		do
			result := input_type (a_routine).type_id
		ensure
			instance_free: class
		end

feature -- Functions

	frozen result_type (a_function: FUNCTION [detachable ANY]): like {FUNCTION [detachable ANY]}.item.generating_type
			-- Type id of the runtime declared return type of `a_function'
		local
			l_type: TYPE [detachable ANY]
		do
			if attached {PREDICATE} a_function then
				result := {BOOLEAN}
			else
				l_type := a_function.generating_type
				result := l_type.generic_parameter_type (l_type.generic_parameter_count)
			end
		ensure
			instance_free: class
		end

	frozen result_type_id (a_function: FUNCTION [detachable ANY]): like {FUNCTION [detachable ANY]}.item.generating_type.type_id
			-- Type id of the runtime declared return type of `a_function'
		do
			result := result_type (a_function).type_id
		ensure
			instance_free: class
		end

feature -- Validation

	frozen invariant_tuple (a_canonical: TUPLE; a_incidental: like a_canonical): BOOLEAN
			-- Whether all the item types in `a_canonical' are matched exactly in `a_incidental'.
			-- `a_incidental' may still have more items.
		require
			conforming: a_incidental.generating_type <= a_canonical.generating_type
		local
			i: like {TUPLE}.Lower
			l_canonical_type, l_incidental_type: TYPE [detachable ANY]
		do
			from
				result := true
				i := a_canonical.Lower
			until
				i > a_canonical.count or not result
			loop
				l_canonical_type := a_canonical.generating_type.generic_parameter_type (i)
				l_incidental_type := a_incidental.generating_type.generic_parameter_type (i)
				if l_canonical_type.is_attached then
						-- Incidental type must be equal
					result := l_canonical_type.type_id = l_incidental_type.type_id
				else
						-- Canonical is detachable, but incidental may still be attached
					result := l_canonical_type.type_id = {ISE_RUNTIME}.detachable_type (l_incidental_type.type_id)
				end
				i := i + 1
			end
		ensure
			true_if_empty: a_canonical.is_empty implies result
			instance_free: class
		end

	frozen same_items (a: TUPLE; b: like a): BOOLEAN
			-- Whether `a' and `b' have the same objects, using reference equality
		local
			i: INTEGER
		do
			if a.count = b.count then
				from
					result := true
					i := a.lower
				until
					i > a.upper or not result
				loop
					result := a[i] = b[i]
					i := i + 1
				end
			end
		ensure
			true_if_empty: (a.is_empty and b.is_empty) implies result
			instance_free: class
		end

	frozen equal_items (a: TUPLE; b: like a): BOOLEAN
			-- Whether `a' and `b' have the same objects, using object equality
		local
			i: INTEGER
		do
			if a.count = b.count then
				from
					result := true
					i := a.lower
				until
					i > a.upper or not result
				loop
					result := a[i] ~ b[i]
					i := i + 1
				end
			end
		ensure
			true_if_empty: (a.is_empty and b.is_empty) implies result
			instance_free: class
		end

end
