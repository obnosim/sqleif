note
	description: "SQLite3 prepared statement that accepts operands"
	author: "obnosim"

class
	PARAMETERIZED_STATEMENT [ARGS -> detachable TUPLE create default_create end]

inherit

	STATEMENT
		redefine
			make_with_flag,
			out
		end

	DATA_TYPES
		rename
			twin as any_twin
		export {NONE} all
		redefine
			out
		end

create
	make, make_long_lived

feature {NONE} -- Initialization

	make_with_flag (a_database: DATABASE; a_query: READABLE_STRING_8; a_flags: like sqlite_prepare_flag_persistent)
			-- <Precursor>
		do
			precursor (a_database, a_query, a_flags)
		ensure then
			valid_operand_types: operand_count = sqlite3_bind_parameter_count (internal_item)
		end

feature -- Output

	out: STRING_8
			-- <Precursor>
		do
			result := statement.to_string_8
			if attached bound_operands as la_operands then
				result.append (" -> ( ")
				result.append (la_operands.out)
				result.append (" ) ")
			end
		end

feature {NONE} -- Implementation

	bound_operands: detachable ARGS
			-- The currently bound operands, if any
		note
			option: stable
		attribute
		end

	to_operand (a_input: detachable ANY): detachable ANY
			-- Persistable form of `a_input' after applying conversions if necessary.
			-- Lookup is done in the following order: `Native_converters', `converters', `database.converters', `Global_converters'.
		do
			if attached a_input then
				if attached {BLOB [ANY]} a_input then
					result := a_input
				elseif attached {CONFIGURATION}.Native_converters.to_operand [a_input] as la_native then
					result := la_native.flexible_item (a_input)
				elseif attached converters.to_operand (a_input) as la_specific then
					result := la_specific.flexible_item (a_input)
				elseif attached database.converters.to_operand (a_input) as la_per_db then
					result := la_per_db.flexible_item (a_input)
				elseif attached {CONFIGURATION}.Global_converters.to_operand (a_input) as la_global then
					result := la_global.flexible_item (a_input)
				elseif fallback_to_blob then
					create {BLOB [ANY]} result.write (a_input)
				else
					check
						no_conversion: False
					then
						do_nothing
					end
				end
			end
		end

	fallback_to_blob: BOOLEAN
			-- Whether at least one layer in the hierarchy is configured to default to blob when an unconfigured type is encountered
		do
			result := converters.default_to_blob or database.converters.default_to_blob or {CONFIGURATION}.Global_converters.default_to_blob
		end

feature -- Access

	current_operands: detachable like bound_operands
			-- The operands currently bound to `Current', if any.
			-- The resulting structure is a copy; mutations will not be reflected back.
		do
			result := bound_operands
			if attached result then
				result := result.twin
			end
		ensure
			copy: attached result implies result /= bound_operands
		end

	operand_count: INTEGER
			-- Total number of operands in `Current'.
			-- Those operands must be bound before `Current' can be applied.
		do
			result := ({ARGS}).generic_parameter_count
		ensure then
			instance_free: class
		end

	bind (a_operands: attached ARGS)
			-- Binds `a_operands' onto `Current'.
			-- Uses runtime types.
		require
			has_operands: operand_count > 0
		local
			i, l_count: INTEGER
			l_value: ANY
		do
			reset
			from
				i := a_operands.Lower
				l_count := operand_count
			until
				i > l_count
			loop
				l_value := to_operand (a_operands [i])
				if not attached l_value then
					sqlite3_bind_null (internal_item, i)
				elseif attached {BOOLEAN_REF} l_value as la_bool then
					sqlite3_bind_int (internal_item, i, la_bool.to_integer)
				elseif attached {INTEGER_8_REF} l_value as la_int then
					sqlite3_bind_int (internal_item, i, la_int.item)
				elseif attached {INTEGER_16_REF} l_value as la_int then
					sqlite3_bind_int (internal_item, i, la_int.item)
				elseif attached {INTEGER_32_REF} l_value as la_int then
					sqlite3_bind_int (internal_item, i, la_int)
				elseif attached {INTEGER_64_REF} l_value as la_int then
					sqlite3_bind_int64 (internal_item, i, la_int)
				elseif attached {NATURAL_8_REF} l_value as la_natural then
					sqlite3_bind_int (internal_item, i, la_natural.item)
				elseif attached {NATURAL_16_REF} l_value as la_natural then
					sqlite3_bind_int (internal_item, i, la_natural.item)
				elseif attached {NATURAL_32_REF} l_value as la_natural then
					sqlite3_bind_int64 (internal_item, i, la_natural.to_integer_64)
				elseif attached {NATURAL_64_REF} l_value as la_natural then
					bind_natural_64 (la_natural, i)
				elseif attached {REAL_32_REF} l_value as la_real then
					sqlite3_bind_double (internal_item, i, la_real.item)
				elseif attached {REAL_64_REF} l_value as la_double then
					sqlite3_bind_double (internal_item, i, la_double)
				elseif attached {CHARACTER_8_REF} l_value as la_char_8 then
					bind_char_8 (la_char_8, i)
				elseif attached {CHARACTER_32_REF} l_value as la_char_32 then
					bind_char_32 (la_char_32, i)
				elseif attached {C_STRING} l_value as la_string then
					sqlite3_bind_text (internal_item, i, la_string.item, la_string.bytes_count)
				elseif attached {BLOB [ANY]} l_value as la_blob then
					sqlite3_bind_blob (internal_item, i, la_blob.pointer, la_blob.bytes_count)
				else
					check
						unsupported: False
					then
						do_nothing
					end
				end
				i := i + 1
				debug ("sqleif")
					database.handle_last_error
				end
			end
			bound_operands := a_operands.twin
		ensure then
			distinct_operands: bound_operands /= a_operands and bound_operands ~ a_operands
			equal_operands: attached bound_operands as la_bound and then {TUPLE_UTIL}.same_items (la_bound, a_operands)
		end

	call alias "()" (a_operands: attached ARGS)
			-- Shorthand for `bind'(`a_operands') and `apply'.
		do
			bind (a_operands)
			apply
		end

feature {NONE} -- External

	frozen sqlite3_bind_null (a_item: like internal_item; a_index: like {TUPLE}.Lower)
			-- Binds a null value to operand # `a_index' of statement `a_item'
		require
			valid: 1 <= a_index and a_index <= operand_count
		external
			"C (sqlite3_stmt*, int) | <sqlite3.h>"
		end

	frozen sqlite3_bind_int (a_item: like internal_item; a_index: like {TUPLE}.Lower; a_value: INTEGER)
			-- Binds `a_value' to operand # `a_index' of statement `a_item'
		require
			valid: 1 <= a_index and a_index <= operand_count
		external
			"C (sqlite3_stmt*, int, int) | <sqlite3.h>"
		end

	frozen sqlite3_bind_int64 (a_item: like internal_item; a_index: like {TUPLE}.Lower; a_value: INTEGER_64)
			-- Binds `a_value' to operand # `a_index' of statement `a_item'
		require
			valid: 1 <= a_index and a_index <= operand_count
		external
			"C (sqlite3_stmt*, int, long) | <sqlite3.h>"
		end

	frozen sqlite3_bind_double (a_item: like internal_item; a_index: like {TUPLE}.Lower; a_value: REAL_64)
			-- Binds `a_value' to operand # `a_index' of statement `a_item'
		require
			valid: 1 <= a_index and a_index <= operand_count
		external
			"C (sqlite3_stmt*, int, double) | <sqlite3.h>"
		end

	frozen bind_natural_64 (a_value: NATURAL_64; a_index: like {TUPLE}.Lower)
			-- Binds `a_value' to operand # `a_index'
		require
			valid: 1 <= a_index and a_index <= operand_count
		do
			sqlite3_bind_int64 (internal_item, a_index, {INT_CONVERTERS}.unsigned_to_signed (a_value))
		end

	frozen bind_char_8 (a_value: CHARACTER_8; a_index: like {TUPLE}.Lower)
			-- Binds `a_value' to operand # `a_index'
		require
			valid: 1 <= a_index and a_index <= operand_count
		do
			bind_text (create {C_STRING}.make (create {STRING_8}.make_filled (a_value, 1)), a_index)
		end

	frozen bind_char_32 (a_value: CHARACTER_32; a_index: like {TUPLE}.Lower)
			-- Binds `a_value' to operand # `a_index'
		require
			valid: 1 <= a_index and a_index <= operand_count
		do
			bind_text (create {C_STRING}.make (create {STRING_32}.make_filled (a_value, 1)), a_index)
		end

	frozen bind_text (a_value: C_STRING; a_index: like {TUPLE}.Lower)
			-- Binds `a_value' to operand # `a_index'
		require
			valid: 1 <= a_index and a_index <= operand_count
		do
			sqlite3_bind_text (internal_item, a_index, a_value.item, a_value.bytes_count)
		end

	frozen sqlite3_bind_text (a_item: like internal_item; a_index: like {TUPLE}.Lower; a_value: like {C_STRING}.item; a_bytes: like {C_STRING}.bytes_count)
			-- Binds `a_bytes' of `a_value' to operand # `a_index' of statement `a_item'
		require
			valid: 1 <= a_index and a_index <= operand_count
		external
			"C inline use <sqlite3.h>"
		alias
			"sqlite3_bind_text($a_item, $a_index, $a_value, $a_bytes, SQLITE_STATIC)"
		end

	frozen sqlite3_bind_blob (a_item: like internal_item; a_index: like {TUPLE}.Lower; a_value: like {BLOB [ANY]}.pointer; a_bytes: like {BLOB [ANY]}.bytes_count)
			-- Binds `a_bytes' of `a_value' to operand # `a_index' of statement `a_item'
		require
			valid: 1 <= a_index and a_index <= operand_count
		external
			"C inline use <sqlite3.h>"
		alias
			"sqlite3_bind_blob($a_item, $a_index, $a_value, $a_bytes, SQLITE_TRANSIENT)"
		end

	frozen sqlite3_bind_parameter_count (a_item: like internal_item): INTEGER
			-- Total number of operands on statement `a_item' as reported by SQLite3
		external
			"C (sqlite3_stmt*) : int | <sqlite3.h>"
		end

end
