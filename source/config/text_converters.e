note
	description: "[
		Basic conversion methods, to be used as lambdas.
		Declared here to facilitate lambda usage without unwittingly capturing targets.
	]"
	author: "obnosim"

expanded class
	TEXT_CONVERTERS

inherit

	C_STRING_CONVERSION
		rename
			c_to_string_8 as to_string_8,
			c_to_immutable_string as to_immutable_string_8,
			c_to_string_32 as to_string_32,
			c_to_immutable_string_32 as to_immutable_string_32
		end

feature -- Basic converters

	frozen c_string_identity (a_value: C_STRING): C_STRING
			-- `a_value'
		do
			result := a_value
		ensure
			identity: result = a_value
			instance_free: class
		end

	frozen self_owning_c_string (a_value: C_STRING): C_STRING
			-- A self-owning copy of `a_value'.
			-- Use this when `a_value' does not own its data and might outlive it
		do
			create result.make_by_pointer (a_value.item)
		ensure
			distinct: result.item /= a_value.item
			copied: result.string.same_string (a_value.string)
			instance_free: class
		end

	frozen to_readable_string_general (a_value: C_STRING): READABLE_STRING_GENERAL
			-- Converts C strings to READABLE_STRING_GENERAL
			-- Uses `to_string_general'
		do
			result := to_string_general (a_value)
		ensure
			instance_free: class
		end

	frozen to_string_general (a_value: C_STRING): STRING_GENERAL
			-- Converts C strings to STRING_GENERAL
			-- Uses `to_string_32'
		do
			result := to_string_32 (a_value)
		ensure
			instance_free: class
		end

	frozen to_readable_string_8 (a_value: C_STRING): READABLE_STRING_8
			-- Converts C strings to READABLE_STRING_8
			-- Uses `to_string_8'
		do
			result := to_string_8 (a_value)
		ensure
			instance_free: class
		end

	frozen to_readable_string_32 (a_value: C_STRING): READABLE_STRING_32
			-- `a_value' converted to a readable string
		do
			result := to_string_32 (a_value)
		ensure
			instance_free: class
		end

	frozen to_immutable_string_general (a_value: C_STRING): IMMUTABLE_STRING_GENERAL
			-- `a_value' converted to an immutable string
			-- `a_value'
		do
			result := to_immutable_string_32 (a_value)
		ensure
			instance_free: class
		end

	frozen to_char_8 (a_value: C_STRING): CHARACTER_8
			-- The single char held by `a_value'
		require
			not_empty: a_value.count > 0
		local
			l_string: STRING_8
		do
			l_string := to_string_8 (a_value)
			check
				is_single_char: l_string.count = 1
			end
			result := l_string.item (l_string.Lower)
		ensure
			instance_free: class
		end

	frozen to_char_32 (a_value: C_STRING): CHARACTER_32
			-- The single char held by `a_value'
		require
			not_empty: a_value.count > 0
		local
			l_string: STRING_32
		do
			l_string := to_string_32 (a_value)
			check
				is_single_char: l_string.count = 1
			end
			result := l_string.item (l_string.Lower)
		ensure
			instance_free: class
		end

	frozen to_char_8_ref (a_value: C_STRING): CHARACTER_8_REF
			-- The single nullable char held by `a_value'
		require
			not_empty: a_value.count > 0
		do
			result := to_char_8 (a_value)
		ensure
			instance_free: class
		end

	frozen to_char_32_ref (a_value: C_STRING): CHARACTER_32_REF
			-- The single nullable char held by `a_value'
		require
			not_empty: a_value.count > 0
		do
			result := to_char_32 (a_value)
		ensure
			instance_free: class
		end

	frozen char_8_to_c (a_value: CHARACTER_8): C_STRING
			-- C string containing only `a_value'
		do
			create result.make (create {STRING_8}.make_filled (a_value, 1))
		ensure
			instance_free: class
		end

	frozen char_32_to_c (a_value: CHARACTER_32): C_STRING
			-- C string containing only `a_value'
		do
			result := string_32_to_c (create {STRING_32}.make_filled (a_value, 1))
		ensure
			instance_free: class
		end

	frozen char_8_ref_to_c (a_value: CHARACTER_8_REF): C_STRING
			-- C string containing only `a_value.item'
		do
			result := char_8_to_c (a_value.item)
		ensure
			instance_free: class
		end

	frozen char_32_ref_to_c (a_value: CHARACTER_32_REF): C_STRING
			-- C string containing only `a_value.item'
		do
			result := char_32_to_c (a_value.item)
		ensure
			instance_free: class
		end

end
