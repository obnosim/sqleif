note
	description: "[
		Holds methods used to convert operands of complex input types to natively supported types
	]"
	author: "obnosim"

deferred class
	READABLE_OPERAND_COERCING_REGISTRY [ORIGINAL -> attached ANY, PERSISTED -> detachable ANY]

inherit

	TUPLE_UTIL
		export
			{NONE} all
			{OPERAND_COERCING_REGISTRY} deep_twin, is_deep_equal, standard_is_equal
		undefine
			default_create
		end

feature {NONE} -- Anchors

	converter_anchor: detachable FUNCTION [TUPLE [ORIGINAL], PERSISTED]
			-- Anchor for the types of items `Current' can provide
		do
			check
				for_anchoring_only: False
			then
				do_nothing
			end
		end

feature -- Access

	exact_item alias "[]" (a_source: ORIGINAL): like converter_anchor
			-- The exact converter to handle values of the type of `a_source', if any.
		deferred
		ensure
			valid: attached result implies result.valid_operands (a_source)
		end

	item alias "()" (a_source: ORIGINAL): like converter_anchor
			-- The most suitable set of converter and binder to handle values of the type of `a_source', if any.
			-- If no exact match is found, tries to find one to which it conforms.
		deferred
		ensure
			valid: attached result implies result.valid_operands (a_source)
		end

	exact_item_for_type (a_type: TYPE [detachable ORIGINAL]): like converter_anchor
			-- The exact converter to handle values of type `a_type', if any.
		deferred
		ensure
			valid: attached result implies result.open_count = 1
		end

	item_for_type (a_type: TYPE [detachable ORIGINAL]): like converter_anchor
			-- The most suitable set of converter and binder to handle values of type `a_type', if any.
			-- If no exact match is found, tries to find one to which it conforms.
		deferred
		ensure
			valid: attached result implies result.open_count = 1
		end

end
