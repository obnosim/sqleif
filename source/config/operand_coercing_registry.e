note
	description: "[
		Holds methods used to convert operands of complex input types to natively supported types
	]"
	author: "obnosim"

class
	OPERAND_COERCING_REGISTRY [ORIGINAL -> attached ANY, PERSISTED -> detachable ANY]

inherit

	READABLE_OPERAND_COERCING_REGISTRY [ORIGINAL, PERSISTED]

feature {NONE} -- Initialization

	default_create
			-- <Precursor>
		do
			create registry.make (3)
			create lock.make
		end

feature {NONE} -- Implementation

	registry: HASH_TABLE [attached like converter_anchor, like input_type_id]
			-- Conversion methods and their associated binding type, indexed by their input type

	lock: READ_WRITE_LOCK
			-- Protects access to `registry'
		attribute
		end

feature {PERSISTENCE_CONFIG} -- Implementation

	register (a_key: TYPE [detachable PERSISTED]; a_converter: attached like converter_anchor)
			-- Uses `a_value' to produce instances of `a_key' from retrieved data
		do
			lock.acquire_write_lock
			registry [{ISE_RUNTIME}.detachable_type (a_key.type_id)] := a_converter
			lock.release_write_lock
		end

feature -- Access

	exact_item alias "[]" (a_source: ORIGINAL): like converter_anchor
			-- <Precursor>
		do
			result := exact_item_for_type (a_source.generating_type)
		end

	item alias "()" (a_source: ORIGINAL): like converter_anchor
			-- <Precursor>
		do
			result := item_for_type (a_source.generating_type)
		end

	exact_item_for_type (a_type: TYPE [detachable ORIGINAL]): like converter_anchor
			-- <Precursor>
		do
			lock.acquire_read_lock
			result := registry [a_type.type_id]
			if not attached result then
				if a_type.is_attached then
					result := registry [{ISE_RUNTIME}.detachable_type (a_type.type_id)]
				else
					result := registry [{ISE_RUNTIME}.attached_type (a_type.type_id)]
				end
			end
			lock.release_read_lock
		end

	item_for_type (a_type: TYPE [detachable ORIGINAL]): like converter_anchor
			-- <Precursor>
		local
			i: like registry.iteration_lower
			l_id: like {TYPE [ORIGINAL]}.type_id
			l_item: like registry.iteration_item
		do
			lock.acquire_read_lock
			if attached exact_item_for_type (a_type) as la_result then
				result := la_result
			else
				l_id := {ISE_RUNTIME}.attached_type (a_type.type_id)
				from
					i := registry.iteration_upper
				until
					attached result or i < registry.iteration_lower
				loop
					l_item := registry.iteration_item (i)
					if {ISE_RUNTIME}.type_conforms_to (l_id, input_type (l_item).type_id) then
						result := l_item
					else
						i := i - 1
					end
				end
			end
			lock.release_read_lock
		end

end
