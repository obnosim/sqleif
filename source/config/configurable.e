note
	description: "Interface allowing configuration of type mappings"
	author: "obnosim"

deferred class
	CONFIGURABLE

inherit

	CONFIG

feature -- Configuration

	configure (a_type: TYPE [detachable ANY]): PERSISTENCE_CONFIG [attached like a_type]
			-- Initializes the configuration of the type `a_type'
		do
			create result.make (store, a_type)
		end

	store_all_other_types_as_blob
			-- Makes it so types for which no conversion was configured are implicitly stored as blob
		do
			store.set_default_to_blob (true)
		end

feature {NONE} -- Implementation

	store: TYPE_CONVERTERS
			-- Registry holding the configurations
		deferred
		end

end
