note
	description: "Conversion methods for float data types"
	author: "obnosim"

expanded class
	FLOAT_CONVERTERS

feature -- Conversion

	frozen to_real_32_ref (a_value: REAL_64): REAL_32_REF
			-- Converts `a_value' to a nullable REAL_32
		require
			within_range: {REAL_32}.Min_value <= a_value and a_value <= {REAL_32}.Max_value
		do
			result := a_value.truncated_to_real
		ensure
			instance_free: class
		end

	frozen real_64_identity (a_value: REAL_64): REAL_64
		do
			result := a_value
		ensure
			identity: result = a_value
			instance_free: class
		end

	frozen to_real_64_ref (a_value: REAL_64): REAL_64_REF
			-- Converts `a_value' to a nullable REAL_64
		do
			result := a_value
		ensure
			instance_free: class
		end

	frozen to_real_32 (a_value: REAL_64): REAL_32
			-- Converts `a_value' to a REAL_32
		require
			within_range: {REAL_32}.Min_value <= a_value and a_value <= {REAL_32}.Max_value
		do
			result := a_value.truncated_to_real
		ensure
			instance_free: class
		end

	frozen real_32_identity (a_value: REAL_32): REAL_32
			-- `a_value'
		do
			result := a_value
		ensure
			identity: result = a_value
			instance_free: class
		end

end
