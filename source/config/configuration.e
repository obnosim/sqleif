note
	description: "Global configuration of the SQLEIF library"
	author: "obnosim"

class
	CONFIGURATION

inherit

	CONFIGURABLE
		rename
			store as Global_converters
		redefine
			configure
		end

feature {CONFIG} -- Implementation

	Native_converters: READABLE_TYPE_CONVERTERS
			-- Converters natively supported by the library.
			-- Take precedence over any other configuration.
		local
			l_int: INT_CONVERTERS
			l_float: FLOAT_CONVERTERS
			l_text: TEXT_CONVERTERS
			l_result: TYPE_CONVERTERS
		once ("process")
			create l_result
			l_result.configure ({BOOLEAN}).store_as_bool (agent l_int.boolean_identity).hydrate (agent l_int.to_boolean)
			l_result.configure ({BOOLEAN_REF}).store_as_bool (agent {BOOLEAN_REF}.item).hydrate (agent l_int.to_boolean_ref)
			l_result.configure ({INTEGER_8}).store_as_integer_8 (agent l_int.int_8_identity).hydrate (agent l_int.to_int_8)
			l_result.configure ({INTEGER_8_REF}).store_as_integer_8 (agent {INTEGER_8_REF}.item).hydrate (agent l_int.to_int_8_ref)
			l_result.configure ({INTEGER_16}).store_as_integer_16 (agent l_int.int_16_identity).hydrate (agent l_int.to_int_16)
			l_result.configure ({INTEGER_16_REF}).store_as_integer_16 (agent {INTEGER_16_REF}.item).hydrate (agent l_int.to_int_16_ref)
			l_result.configure ({INTEGER_32}).store_as_integer_32 (agent l_int.int_32_identity).hydrate (agent l_int.to_int_32)
			l_result.configure ({INTEGER_32_REF}).store_as_integer_32 (agent {INTEGER_32_REF}.item).hydrate (agent l_int.to_int_32_ref)
			l_result.configure ({INTEGER_64}).store_as_integer_64 (agent l_int.int_64_identity).hydrate (agent l_int.int_64_identity)
			l_result.configure ({INTEGER_64_REF}).store_as_integer_64 (agent {INTEGER_64_REF}.item).hydrate (agent l_int.to_int_64_ref)
			l_result.configure ({NATURAL_8}).store_as_natural_8 (agent l_int.natural_8_identity).hydrate (agent l_int.to_natural_8)
			l_result.configure ({NATURAL_8_REF}).store_as_natural_8 (agent {NATURAL_8_REF}.item).hydrate (agent l_int.to_natural_8_ref)
			l_result.configure ({NATURAL_16}).store_as_natural_16 (agent l_int.natural_16_identity).hydrate (agent l_int.to_natural_16)
			l_result.configure ({NATURAL_16_REF}).store_as_natural_16 (agent {NATURAL_16_REF}.item).hydrate (agent l_int.to_natural_16_ref)
			l_result.configure ({NATURAL_32}).store_as_natural_32 (agent l_int.natural_32_identity).hydrate (agent l_int.to_natural_32)
			l_result.configure ({NATURAL_32_REF}).store_as_natural_32 (agent {NATURAL_32_REF}.item).hydrate (agent l_int.to_natural_32_ref)
			l_result.configure ({NATURAL_64}).store_as_natural_64 (agent l_int.natural_64_identity).hydrate (agent l_int.to_natural_64)
			l_result.configure ({NATURAL_64_REF}).store_as_natural_64 (agent {NATURAL_64_REF}.item).hydrate (agent l_int.to_natural_64_ref)
			l_result.configure ({REAL_32}).store_as_real_32 (agent l_float.real_32_identity).hydrate (agent l_float.to_real_32)
			l_result.configure ({REAL_32_REF}).store_as_real_32 (agent {REAL_32_REF}.item).hydrate (agent l_float.to_real_32_ref)
			l_result.configure ({REAL_64}).store_as_real_64 (agent l_float.real_64_identity).hydrate (agent l_float.real_64_identity)
			l_result.configure ({REAL_64_REF}).store_as_real_64 (agent {REAL_64_REF}.item).hydrate (agent l_float.to_real_64_ref)
			l_result.configure ({CHARACTER_8}).store_as_text (agent l_text.char_8_to_c).hydrate (agent l_text.to_char_8)
			l_result.configure ({CHARACTER_8_REF}).store_as_text (agent l_text.char_8_ref_to_c).hydrate (agent l_text.to_char_8_ref)
			l_result.configure ({CHARACTER_32}).store_as_text (agent l_text.char_32_to_c).hydrate (agent l_text.to_char_32)
			l_result.configure ({CHARACTER_32_REF}).store_as_text (agent l_text.char_32_ref_to_c).hydrate (agent l_text.to_char_32_ref)
			l_result.configure ({READABLE_STRING_GENERAL}).store_as_text (agent l_text.readable_string_general_to_c).hydrate (agent l_text.to_string_32)
			l_result.configure ({READABLE_STRING_8}).store_as_text (agent l_text.readable_string_8_to_c).hydrate (agent l_text.to_string_8)
			l_result.configure ({READABLE_STRING_32}).store_as_text (agent l_text.readable_string_32_to_c).hydrate (agent l_text.to_string_32)
			l_result.configure ({IMMUTABLE_STRING_GENERAL}).store_as_text (agent l_text.immutable_string_general_to_c).hydrate (agent l_text.to_immutable_string_32)
			l_result.configure ({IMMUTABLE_STRING_8}).store_as_text (agent l_text.immutable_string_8_to_c).hydrate (agent l_text.to_immutable_string_8)
			l_result.configure ({IMMUTABLE_STRING_32}).store_as_text (agent l_text.immutable_string_32_to_c).hydrate (agent l_text.to_immutable_string_32)
			l_result.configure ({STRING_GENERAL}).store_as_text (agent l_text.string_general_to_c).hydrate (agent l_text.to_string_32)
			l_result.configure ({STRING_8}).store_as_text (agent l_text.string_8_to_c).hydrate (agent l_text.to_string_8)
			l_result.configure ({STRING_32}).store_as_text (agent l_text.string_32_to_c).hydrate (agent l_text.to_string_32)
			l_result.configure ({C_STRING}).store_as_text (agent l_text.c_string_identity).hydrate (agent l_text.self_owning_c_string)
			result := l_result
		ensure
			instance_free: class
		end

	Global_converters: TYPE_CONVERTERS
			-- Custom converters, shared by all databases in the application.
			-- Can be overridden on a per-database basis via `converters'.
		once ("process")
			create result
		ensure then
			instance_free: class
		end

feature -- Configuration

	configure (a_type: TYPE [detachable ANY]): PERSISTENCE_CONFIG [like a_type]
			-- <Precursor>
			-- Applies to all databases in the application, but can be overridden by individual databases.
		do
			create result.make (Global_converters, a_type)
		ensure then
			instance_free: class
		end

end
