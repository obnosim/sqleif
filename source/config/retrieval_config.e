note
	description: "Configures the method to use to rehydrate a persisted type"
	author: "obnosim"

class
	RETRIEVAL_CONFIG [PERSISTED -> attached ANY, HYDRATED -> TYPE [detachable ANY]]

inherit

	CONFIG
		rename
			do_nothing as done
		end

	TUPLE_UTIL
		rename
			do_nothing as done
		export
			{NONE} all
			{TYPE_CONVERTERS} deep_twin, is_deep_equal, standard_is_equal
		end

create {PERSISTENCE_CONFIG}
	make

feature {NONE} -- Initialization

	make (a_store: like store; a_type: HYDRATED)
			-- `Current' shall configure retrieval of `a_type' into `a_store'
		do
			store := a_store
			type := a_type
		end

feature {NONE} -- Implementation

	store: RETRIEVAL_CONVERSION_REGISTRY [PERSISTED, detachable ANY]
			-- Registry for the configuration

	type: HYDRATED
			-- Target type

feature -- Configuration

	hydrate (a_hydrater: FUNCTION [PERSISTED, attached like {HYDRATED}.default])
			-- Makes `a_hydrater' usable to restore persisted data
		do
			store.register (type, a_hydrater)
		end

end
