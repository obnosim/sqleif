note
	description: "Configures the method to use to persist a type"
	author: "obnosim"

class
	PERSISTENCE_CONFIG [T -> TYPE [detachable ANY]]

inherit

	CONFIG
		rename
			do_nothing as done
		end

	TUPLE_UTIL
		rename
			do_nothing as done
		export
			{NONE} all
			{PERSISTENCE_CONFIG} deep_twin, is_deep_equal, standard_is_equal
		end

create {CONFIGURABLE}
	make

feature {NONE} -- Initialization

	make (a_store: like store; a_type: T)
			-- `Current' shall configure persistence of `a_type' into `a_store'
		do
			store := a_store
			type := a_type
		end

feature {NONE} -- Implementation

	store: TYPE_CONVERTERS
			-- Registry for the configuration

	type: T
			-- Target type

feature -- Config: integers

	store_as_bool (a_mapper: FUNCTION [attached like type.default, detachable BOOLEAN_REF]): RETRIEVAL_CONFIG [INTEGER_64_REF, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to booleans
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_int, type)
		end

	store_as_integer_8 (a_mapper: FUNCTION [attached like type.default, detachable INTEGER_8_REF]): RETRIEVAL_CONFIG [INTEGER_64_REF, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to booleans
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_int, type)
		end

	store_as_integer_16 (a_mapper: FUNCTION [attached like type.default, detachable INTEGER_16_REF]): RETRIEVAL_CONFIG [INTEGER_64_REF, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to booleans
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_int, type)
		end

	store_as_integer_32 (a_mapper: FUNCTION [attached like type.default, detachable INTEGER_32_REF]): RETRIEVAL_CONFIG [INTEGER_64_REF, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to booleans
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_int, type)
		end

	store_as_integer_64 (a_mapper: FUNCTION [attached like type.default, detachable INTEGER_64_REF]): RETRIEVAL_CONFIG [INTEGER_64_REF, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to booleans
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_int, type)
		end

feature -- Config: naturals

	store_as_natural_8 (a_mapper: FUNCTION [attached like type.default, detachable NATURAL_8_REF]): RETRIEVAL_CONFIG [INTEGER_64_REF, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to booleans
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_int, type)
		end

	store_as_natural_16 (a_mapper: FUNCTION [attached like type.default, detachable NATURAL_16_REF]): RETRIEVAL_CONFIG [INTEGER_64_REF, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to booleans
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_int, type)
		end

	store_as_natural_32 (a_mapper: FUNCTION [attached like type.default, detachable NATURAL_32_REF]): RETRIEVAL_CONFIG [INTEGER_64_REF, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to booleans
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_int, type)
		end

	store_as_natural_64 (a_mapper: FUNCTION [attached like type.default, detachable NATURAL_64_REF]): RETRIEVAL_CONFIG [INTEGER_64_REF, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to {NATURAL_64}
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_int, type)
		end

feature -- Config: floats

	store_as_real_32 (a_mapper: FUNCTION [attached like type.default, detachable REAL_32_REF]): RETRIEVAL_CONFIG [REAL_64_REF, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to {NATURAL_64}
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_float, type)
		end

	store_as_real_64 (a_mapper: FUNCTION [attached like type.default, detachable REAL_64_REF]): RETRIEVAL_CONFIG [REAL_64_REF, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to {NATURAL_64}
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_float, type)
		end

feature -- Config: text

	store_as_text (a_mapper: FUNCTION [attached like type.default, detachable C_STRING]): RETRIEVAL_CONFIG [C_STRING, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to {C_STRING}
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_text, type)
		end

	store_as_char_8 (a_mapper: FUNCTION [attached like type.default, detachable CHARACTER_8_REF]): RETRIEVAL_CONFIG [C_STRING, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to {CHARACTER_8_REF}
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_text, type)
		end

	store_as_char_32, store_as_wide_char (a_mapper: FUNCTION [attached like type.default, detachable CHARACTER_32_REF]): RETRIEVAL_CONFIG [C_STRING, T]
			-- Statements receiving instances of `{T}' as arguments shall use `a_mapper' to turn them to {CHARACTER_32_REF}
		do
			store.to_operand.register (type, a_mapper)
			create result.make (store.from_text, type)
		end

feature -- Config: blob

	store_as_blob
			-- Statements receiving instances of `{T}' as arguments shall implicitly treat them as blob data.
			-- See {BLOB} for caveats with regards to `{T}'.
		do
			store.to_operand.register (type, agent  (a_data: attached ANY): BLOB [attached ANY]
				do
					create result.write (a_data)
				end)
		end

end
