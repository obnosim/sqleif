note
	description: "[
		Provides access to the most suitable registered conversion method for a given type.
	]"
	author: "obnosim"

class
	RETRIEVAL_CONVERSION_REGISTRY [PERSISTED -> attached ANY, HYDRATED -> detachable ANY]

inherit

	READABLE_RETRIEVAL_CONVERSION_REGISTRY [PERSISTED, HYDRATED]

feature {NONE} -- Initialization

	default_create
			-- <Precursor>
		do
			create registry.make (1)
			registry.compare_objects
			create lock.make
		end

feature {NONE} -- Implementation

	registry: HASH_TABLE [FUNCTION [PERSISTED, detachable HYDRATED], like {TYPE [ANY]}.type_id]
			-- Holds conversion methods, indexed by their runtime declared return type

	lock: READ_WRITE_LOCK
			-- Protects access to `registry'
		attribute
		end

feature {CONFIG} -- Access

	register (a_output_type: TYPE [detachable HYDRATED]; a_converter: FUNCTION [PERSISTED, detachable HYDRATED])
			-- Adds `a_converter' as a known conversion method from `{IN}' to `a_output_type'.
		require
			valid_converter: is_valid (a_converter, a_output_type)
		do
			lock.acquire_write_lock
			registry [a_output_type.type_id] := a_converter
			lock.release_write_lock
		ensure
			registered: exact_item (a_output_type) = a_converter
			no_conflict: not registry.conflict
		end

feature {CONFIG} -- Access

	exact_item alias "[]" (a_type: TYPE [detachable HYDRATED]): detachable FUNCTION [PERSISTED, detachable HYDRATED]
			-- <Precursor>
		do
			lock.acquire_read_lock
			result := registry [a_type.type_id]
			if not attached result then
				if a_type.is_attached then
					result := registry [{ISE_RUNTIME}.detachable_type (a_type.type_id)]
				else
					result := registry [{ISE_RUNTIME}.attached_type (a_type.type_id)]
				end
			end
			lock.release_read_lock
		end

	item alias "()" (a_type: TYPE [detachable HYDRATED]): detachable FUNCTION [PERSISTED, detachable HYDRATED]
			-- <Precursor>
		local
			l_type_id: like {TYPE [ANY]}.type_id
		do
			lock.acquire_read_lock
			result := exact_item (a_type)
			if not attached result then
					-- No suitable converter producing instances of the exact type was registered.
					-- Find the first converter producing values conforming to the type, if any.
				l_type_id := a_type.type_id
				across
					registry.new_cursor.reversed as i
				until
					attached result
				loop
					if {ISE_RUNTIME}.type_conforms_to (result_type_id (i.item), l_type_id) then
						result := i.item
					end
				end
			end
			lock.release_read_lock
		end

end
