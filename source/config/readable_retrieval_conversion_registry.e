note
	description: "[
		Provides access to the most suitable registered conversion method for a given type.
	]"
	author: "obnosim"

deferred class
	READABLE_RETRIEVAL_CONVERSION_REGISTRY [PERSISTED -> attached ANY, HYDRATED -> detachable ANY]

inherit

	TUPLE_UTIL
		export
			{NONE} all
			{RETRIEVAL_CONVERSION_REGISTRY} deep_twin, is_deep_equal, standard_is_equal
		undefine
			default_create
		end

feature {CONFIG} -- Access

	is_valid (a_converter: FUNCTION [PERSISTED, detachable HYDRATED]; a_output_type: TYPE [detachable HYDRATED]): BOOLEAN
			-- Whether `a_converter' is valid for `Current'
		do
			check
				one_operand: a_converter.open_count = 1
				outputs_desired_type: attached {PREDICATE} a_converter implies a_output_type.type_id = ({BOOLEAN}).type_id
				outputs_desired_type: not attached {PREDICATE} a_converter implies {ISE_RUNTIME}.type_conforms_to (result_type_id (a_converter), a_output_type.type_id)
			then
				result := true
			end
		end

feature {CONFIG} -- Access

	exact_item alias "[]" (a_type: TYPE [detachable HYDRATED]): detachable FUNCTION [PERSISTED, detachable HYDRATED]
			-- The exact converter to produce instances of `a_type' from `{PERSISTED}', if any.
		deferred
		ensure
			callable_if_any: attached result implies result.open_count = 1
			suitable_if_any: attached result implies {ISE_RUNTIME}.type_conforms_to (result_type_id (result), a_type.type_id)
			invariant_arg: attached result implies input_type_id (result) = ({PERSISTED}).type_id
		end

	item alias "()" (a_type: TYPE [detachable HYDRATED]): detachable FUNCTION [PERSISTED, detachable HYDRATED]
			-- The most suitable converter to produce instances of `a_type' from `{PERSISTED}', if any.
			-- The order of priority is as such:
			-- 1 - Exact match: an entry producing instances of `a_type' exactly
			-- 2 - Conforming match: the first entry, in the reverse order in which they were registered, that produces results conforming to `a_type'
		deferred
		ensure
			callable_if_any: attached result implies result.open_count = 1
			suitable_if_any: attached result implies {ISE_RUNTIME}.type_conforms_to (result_type_id (result), a_type.type_id)
			invariant_arg: attached result implies input_type_id (result) = ({PERSISTED}).type_id
		end

end
