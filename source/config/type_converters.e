note
	description: "Groups a set of conversion registries together"
	author: "obnosim"

class
	TYPE_CONVERTERS

inherit

	READABLE_TYPE_CONVERTERS

create {CONFIG}
	default_create

feature {NONE} -- Initialization

	default_create
			-- <Precursor>
		do
			create from_int
			create from_float
			create from_text
			create to_operand
		end

feature {NONE} -- Implementation

	store: TYPE_CONVERTERS
			-- <Precursor>
		do
			result := current
		end

feature -- Retrieval

	from_int: RETRIEVAL_CONVERSION_REGISTRY [INTEGER_64, detachable ANY]
			-- <Precursor>
		attribute
		end

	from_float: RETRIEVAL_CONVERSION_REGISTRY [REAL_64, detachable ANY]
			-- <Precursor>
		attribute
		end

	from_text: RETRIEVAL_CONVERSION_REGISTRY [C_STRING, detachable ANY]
			-- <Precursor>
		attribute
		end

feature -- Binding

	to_operand: OPERAND_COERCING_REGISTRY [ANY, detachable ANY]
			-- <Precursor>
		attribute
		end

feature {CONFIGURABLE} -- Configuration

	default_to_blob: BOOLEAN assign set_default_to_blob
			-- Whether unconfigured types should be stored as blob instead of throwing exceptions

	set_default_to_blob (a_state: like default_to_blob)
			-- Assigns `a_state' to `default_to_blob'
		do
			default_to_blob := a_state
		ensure
			default_to_blob
		end

end
