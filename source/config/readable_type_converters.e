note
	description: "Groups a set of conversion registries together"
	author: "obnosim"

deferred class
	READABLE_TYPE_CONVERTERS

inherit

	CONFIGURABLE
		undefine
			default_create
		end

feature -- Retrieval

	from_int: READABLE_RETRIEVAL_CONVERSION_REGISTRY [INTEGER_64, detachable ANY]
			-- Used to map values retrieved from int fields into other types
		deferred
		end

	from_float: READABLE_RETRIEVAL_CONVERSION_REGISTRY [REAL_64, detachable ANY]
			-- Used to map values retrieved from float fields into other types
		deferred
		end

	from_text: READABLE_RETRIEVAL_CONVERSION_REGISTRY [C_STRING, detachable ANY]
			-- Used to map values retrieved from text fields into other types
		deferred
		end

feature -- Binding

	to_operand: READABLE_OPERAND_COERCING_REGISTRY [ANY, detachable ANY]
			-- Used to map various types of data into the corresponding operand type
		deferred
		end

end
