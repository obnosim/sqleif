note
	description: "[
		Methods to handle integers and naturals and convert integer data types.
		Special treatment is used on NATURAL_64 as they are too large to fit a SQLite int column.
	]"
	author: "obnosim"

expanded class
	INT_CONVERTERS

feature {NONE} -- Constants

	Sign_bit_index: INTEGER = 63
			-- Index of the sign bit within 64-bit integers

feature -- Natural handling

	frozen unsigned_to_signed (a_value: NATURAL_64): INTEGER_64
			-- The bits of `a_value', interpreted as a signed integer.
			-- If `a_value' is too large to fit a signed int, its sign bit will be used and the result will be negative.
		do
			if a_value.bit_test (Sign_bit_index) then
					-- Too big
				result := a_value.set_bit (false, Sign_bit_index).to_integer_64.set_bit (true, Sign_bit_index)
			else
				result := a_value.to_integer_64
			end
		ensure
			negative_if_too_large: (a_value > result.Max_value.as_natural_64) = (result < result.zero)
			instance_free: class
		end

	frozen signed_to_unsigned (a_value: like unsigned_to_signed): NATURAL_64
			-- The bits of `a_value', interpreted as a natural.
			-- If `a_value' is negative, the sign bit will be treated as a regular bit to produce as bigger result.
		do
			if a_value.bit_test (Sign_bit_index) then
					-- Negative: the original unsigned value was too big for a signed int
				result := a_value.set_bit (false, Sign_bit_index).to_natural_64.set_bit (true, Sign_bit_index)
			else
				result := a_value.to_natural_64
			end
		ensure
			corrected_if_negative: (a_value < a_value.zero) = (result > a_value.Max_value.as_natural_64)
			instance_free: class
		end

feature -- Conversion

	frozen boolean_identity (a_value: BOOLEAN): BOOLEAN
			-- `a_value'
		do
			result := a_value
		ensure
			identity: result = a_value
			instance_free: class
		end

	frozen to_boolean_ref (a_value: INTEGER_64): BOOLEAN_REF
			-- Converts `a_value' into a nullable BOOLEAN
		do
			result := a_value.to_boolean
		ensure
			instance_free: class
		end

	frozen to_int_8_ref (a_value: INTEGER_64): INTEGER_8_REF
			-- Converts `a_value' into a nullable INTEGER_8
		require
			within_range: {INTEGER_8}.Min_value <= a_value and a_value <= {INTEGER_8}.Max_value
		do
			result := a_value.to_integer_8
		ensure
			same_value: result.item = a_value
			instance_free: class
		end

	frozen int_8_identity (a_value: INTEGER_8): INTEGER_8
		do
			result := a_value
		ensure
			identity: result = a_value
			instance_free: class
		end

	frozen to_int_16_ref (a_value: INTEGER_64): INTEGER_16_REF
			-- Converts `a_value' into a nullable INTEGER_16
		require
			within_range: {INTEGER_16}.Min_value <= a_value and a_value <= {INTEGER_16}.Max_value
		do
			result := a_value.to_integer_16
		ensure
			same_value: result.item = a_value
			instance_free: class
		end

	frozen int_16_identity (a_value: INTEGER_16): INTEGER_16
			-- `a_value'
		do
			result := a_value
		ensure
			identity: result = a_value
			instance_free: class
		end

	frozen to_int_32_ref (a_value: INTEGER_64): INTEGER_32_REF
			-- Converts `a_value' into a nullable INTEGER_32
		require
			within_range: {INTEGER_32}.Min_value <= a_value and a_value <= {INTEGER_32}.Max_value
		do
			result := a_value.to_integer_32
		ensure
			same_value: result.item = a_value
			instance_free: class
		end

	frozen int_32_identity (a_value: INTEGER_32): INTEGER_32
			-- `a_value'
		do
			result := a_value
		ensure
			identity: result = a_value
			instance_free: class
		end

	frozen to_int_64_ref (a_value: INTEGER_64): INTEGER_64_REF
			-- Converts `a_value' into a nullable INTEGER_64
		require
			positive: a_value > a_value.zero
		do
			result := a_value
		ensure
			same_value: result.item = a_value
			instance_free: class
		end

	frozen int_64_identity (a_value: INTEGER_64): INTEGER_64
			-- `a_value'
		do
			result := a_value
		ensure
			identity: result = a_value
			instance_free: class
		end

	frozen to_natural_8_ref (a_value: INTEGER_64): NATURAL_8_REF
			-- Converts `a_value' into a nullable NATURAL_8
		require
			within_range: a_value.zero <= a_value and a_value <= {NATURAL_8}.Max_value
		do
			result := a_value.to_natural_8
		ensure
			same_value: result.item = a_value
			instance_free: class
		end

	frozen natural_8_identity (a_value: NATURAL_8): NATURAL_8
			-- `a_value'
		do
			result := a_value
		ensure
			identity: result = a_value
			instance_free: class
		end

	frozen to_natural_16_ref (a_value: INTEGER_64): NATURAL_16_REF
			-- Converts `a_value' into a nullable NATURAL_16
		require
			within_range: a_value.zero <= a_value and a_value <= {NATURAL_16}.Max_value
		do
			result := a_value.to_natural_16
		ensure
			same_value: result.item = a_value
			instance_free: class
		end

	frozen natural_16_identity (a_value: NATURAL_16): NATURAL_16
			-- `a_value'
		do
			result := a_value
		ensure
			identity: result = a_value
			instance_free: class
		end

	frozen to_natural_32_ref (a_value: INTEGER_64): NATURAL_32_REF
			-- Converts `a_value' into a nullable NATURAL_32
		require
			within_range: a_value.zero <= a_value and a_value <= {NATURAL_32}.Max_value
		do
			result := a_value.to_natural_32
		ensure
			same_value: result.item = a_value
			instance_free: class
		end

	frozen natural_32_identity (a_value: NATURAL_32): NATURAL_32
			-- `a_value'
		do
			result := a_value
		ensure
			identity: result = a_value
			instance_free: class
		end

	frozen to_natural_64_ref (a_value: INTEGER_64): NATURAL_64_REF
			-- Converts `a_value' into a nullable NATURAL_64.
			-- If `a_value' is negative, its sign bit will be considered as a regular bit.
		do
			result := signed_to_unsigned (a_value)
		ensure
			same_value: unsigned_to_signed (result.item) = a_value
			instance_free: class
		end

	frozen natural_64_identity (a_value: NATURAL_64): NATURAL_64
			-- `a_value'
		do
			result := a_value
		ensure
			identity: result = a_value
			instance_free: class
		end

	frozen to_boolean (a_value: INTEGER_64): BOOLEAN
			-- Converts `a_value' into a nullable BOOLEAN
		do
			result := a_value.to_boolean
		ensure
			instance_free: class
		end

	frozen to_int_8 (a_value: INTEGER_64): INTEGER_8
			-- Converts `a_value' into a nullable INTEGER_8
		require
			within_range: {INTEGER_8}.Min_value <= a_value and a_value <= {INTEGER_8}.Max_value
		do
			result := a_value.to_integer_8
		ensure
			same_value: result.item = a_value
			instance_free: class
		end

	frozen to_int_16 (a_value: INTEGER_64): INTEGER_16
			-- Converts `a_value' into a nullable INTEGER_16
		require
			within_range: {INTEGER_16}.Min_value <= a_value and a_value <= {INTEGER_16}.Max_value
		do
			result := a_value.to_integer_16
		ensure
			same_value: result.item = a_value
			instance_free: class
		end

	frozen to_int_32 (a_value: INTEGER_64): INTEGER_32
			-- Converts `a_value' into a nullable INTEGER_32
		require
			within_range: {INTEGER_32}.Min_value <= a_value and a_value <= {INTEGER_32}.Max_value
		do
			result := a_value.to_integer_32
		ensure
			same_value: result.item = a_value
			instance_free: class
		end

	frozen to_natural_8 (a_value: INTEGER_64): NATURAL_8
			-- Converts `a_value' into a nullable NATURAL_8
		require
			within_range: a_value.zero <= a_value and a_value <= {NATURAL_8}.Max_value
		do
			result := a_value.to_natural_8
		ensure
			same_value: result.item = a_value
			instance_free: class
		end

	frozen to_natural_16 (a_value: INTEGER_64): NATURAL_16
			-- Converts `a_value' into a nullable NATURAL_16
		require
			within_range: a_value.zero <= a_value and a_value <= {NATURAL_16}.Max_value
		do
			result := a_value.to_natural_16
		ensure
			same_value: result.item = a_value
			instance_free: class
		end

	frozen to_natural_32 (a_value: INTEGER_64): NATURAL_32
			-- Converts `a_value' into a nullable NATURAL_32
		require
			within_range: a_value.zero <= a_value and a_value <= {NATURAL_32}.Max_value
		do
			result := a_value.to_natural_32
		ensure
			same_value: result.item = a_value
			instance_free: class
		end

	frozen to_natural_64 (a_value: INTEGER_64): NATURAL_64
			-- Converts `a_value' into a nullable NATURAL_64.
			-- If `a_value' is negative, its sign bit will be considered as a regular bit.
		do
			result := signed_to_unsigned (a_value)
		ensure
			same_value: unsigned_to_signed (result.item) = a_value
			instance_free: class
		end

end
