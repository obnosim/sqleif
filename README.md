# sqleif
#### Basic SQLite3 wrapper library for Eiffel

Designed for ease of use, with a convenient and simple API.

Only the basic functionalities of SQLite are currently supported. It is probably too simplistic for applications centered around their database(s), but it can be useful for applications that just need a place to persist their data.

Unlike other, more mature libraries, sqleif makes extensive use of typed TUPLEs to reduce the client's burden and keep the code short, legible and expressive while still being strongly typed.

```eiffel
local
	db: DATABASE
do
	create db.open_read ("storage/example.db")

		-- Make custom types known to the library for automatic conversion
	db.converters.from_int.register (agent (a_type_code: INTEGER_64) : USER_TYPE
		do
			create result.from_code (a_type_code)
		end)

		-- Prepared statement are generically typed to statically check
		-- operands and result types based on the tuples passed at creation time
	across
		db.query ("SELECT id, first_name, type FROM users WHERE language = ?1 AND is_active = ?2;", ["FR", true], create {TUPLE [id: NATURAL; first_name: STRING; type: USER_TYPE]})
	is
		user
	loop
			-- The results are presented in a tuple matching the one used to create the query
			-- thus field names can be used:
		print("%N id: " + user.id.out)             -- id is a NATURAL
		print("%N first_name: " + user.first_name) -- first_name is a STRING
		print("%N type: " + user.type.out)         -- type is a user-defined type
	end
end
```

In addition to the basic types supported natively by the library, client applications can register converters for any other type (except pointers). In the example above, USER_TYPE is a hypothetical enumeration type that can be created from an INTEGER_64. By registering a converter up front, clients avoid having to do the conversion manually, without having to limit themselves to primitive types.

Eiffel's built-in object storage and retrieval is also supported (using SQLite3's blob type):

```eiffel
local
	db: DATABASE
	my_object, my_retrieved_object: MY_OBJECT
	insert_statement: PARAMETERIZED_STATEMENT [TUPLE [id: INTEGER; content: BLOB [MY_OBJECT]]]
	select_statement: PARAMETERIZED_QUERY [TUPLE [id: INTEGER], TUPLE [content: BLOB [MY_OBJECT]]]
do
	create db.open_write ("storage/example.db")

	create insert_statement.make (db, "INSERT INTO my_blobs (id, content) VALUES (?1, ?2);")

	create my_object.make

	insert_statement.apply (1, create {BLOB [MY_OBJECT]}.write (my_object))
	-- A serialized form of `my_object' is now stored as blob

	create select_statement.make (db, "SELECT content FROM my_blobs WHERE id = ?;")
	select_statement.apply (1)
	check not select_statement.exhausted then
		my_retrieved_object := select_statement.item.blob.item
		check
			retrieved_successfully: my_object ~ my_retrieved_object
			-- The retrieved object matches the original perfectly and is
			-- presented in a strongly typed way.
		end
	end
end
```

Implicit blob storage can also be configured for any type:

```eiffel
local
	db: DATABASE
	my_object, my_retrieved_object: MY_OBJECT
	insert_statement: PARAMETERIZED_STATEMENT [TUPLE [id: INTEGER; content: MY_OBJECT]]
	select_statement: PARAMETERIZED_QUERY [TUPLE [id: INTEGER], TUPLE [content: MY_OBJECT]]
do
	create db.open_write ("storage/example.db")

		-- Tell the database object to implicitly store instances of MY_OBJECT as blob
	db.configure ({MY_OBJECT}).store_as_blob

	create my_object.make
	db.execute ("INSERT INTO my_blobs (id, content) VALUES (?1, ?2);", 1, my_object)

	create select_statement.make (db, "SELECT content FROM my_blobs WHERE id = ?;")
	check
	    retrieved_successfully: my_object ~ select_statement.single (1).content
	end
end
```
