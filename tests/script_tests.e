note
	description: "Tests for SCRIPT"
	author: "obnosim"

class
	SCRIPT_TESTS

inherit

	BASE_TEST
		redefine
			on_prepare
		end

feature {NONE} -- Implementation

	on_prepare
		local
			l_test_name: STRING
		do
			precursor
			l_test_name := test_name
			db.execute ("CREATE TABLE " + l_test_name + " " + "[
				(
					id		INTEGER	NOT NULL	PRIMARY KEY,
					name	TEXT	NOT NULL
				);
			]", void)
		end

feature -- Execution tests

	when_multiple_instructions_then_executed_one_at_a_time
		local
			l_test_name: STRING
			l_statement_text: STRING
			l_statement: SCRIPT
			l_query: QUERY [TUPLE [name: STRING]]
			l_inserted: ARRAYED_LIST [TUPLE [name: STRING]]
			j: INTEGER
		do
			across
				1 |..| 5 is n
			from
				l_test_name := test_name
				l_statement_text := ""
			loop
				l_statement_text := l_statement_text + "%NINSERT INTO " + l_test_name + " (name) VALUES " + "(" + n.out + ");"
			end
			create l_statement.make (db, l_statement_text)

			from
				j := 0
				l_query := db.query ("SELECT name from " + l_test_name + " order by name asc;", void, create {TUPLE [STRING]})
			until
				j >= 5
			loop
				l_statement.step
				j := j + 1
				l_inserted := l_query.list
				assert ("all inserted", l_inserted.count = j)
				across
					1 |..| j is i
				loop
					assert (i.out + " inserted: " + l_inserted [i].name, l_inserted [i].name ~ i.out)
				end
			end
			assert ("exhausted", l_statement.after)
		end

	when_run_till_end_then_all_instructions_run_at_once
		local
			l_test_name: STRING
			l_statement_text: STRING
			l_statement: SCRIPT
			l_query: QUERY [TUPLE [name: STRING]]
			l_inserted: ARRAYED_LIST [TUPLE [name: STRING]]
		do
			across
				1 |..| 5 is n
			from
				l_test_name := test_name
				l_statement_text := ""
			loop
				l_statement_text := l_statement_text + "%NINSERT INTO " + l_test_name + " (name) VALUES " + "(" + n.out + ");"
			end
			create l_statement.make (db, l_statement_text)
			l_statement.run_till_end
			assert ("exhausted", l_statement.after)

			l_query := db.query ("SELECT name from " + l_test_name + " order by name asc;", void, create {TUPLE [STRING]})
			l_inserted := l_query.list
			assert ("all inserted", l_inserted.count = 5)
			across
				1 |..| 5 is i
			loop
				assert (i.out + " inserted: " + l_inserted [i].name, l_inserted [i].name ~ i.out)
			end
		end

	when_called_polymorphically_as_statement_then_all_instructions_run_at_once
		local
			l_test_name: STRING
			l_statement_text: STRING
			l_statement: STATEMENT
			l_query: QUERY [TUPLE [name: STRING]]
			l_inserted: ARRAYED_LIST [TUPLE [name: STRING]]
		do
			across
				1 |..| 5 is n
			from
				l_test_name := test_name
				l_statement_text := ""
			loop
				l_statement_text := l_statement_text + "%NINSERT INTO " + l_test_name + " (name) VALUES " + "(" + n.out + ");"
			end
			create {SCRIPT} l_statement.make (db, l_statement_text)
			l_statement.apply
			assert ("exhausted", l_statement.after)

			l_query := db.query ("SELECT name from " + l_test_name + " order by name asc;", void, create {TUPLE [STRING]})
			l_inserted := l_query.list
			assert ("all inserted", l_inserted.count = 5)
			across
				1 |..| 5 is i
			loop
				assert (i.out + " inserted: " + l_inserted [i].name, l_inserted [i].name ~ i.out)
			end
		end

	when_reset_then_restarts_from_beginning
		local
			l_test_name: STRING
			l_statement_text: STRING
			l_statement: SCRIPT
			l_query: QUERY [TUPLE [id: INTEGER]]
			l_inserted: ARRAYED_LIST [TUPLE [id: INTEGER]]
		do
			across
				1 |..| 5 is n
			from
				l_test_name := test_name
				l_statement_text := ""
			loop
				l_statement_text := l_statement_text + "%NINSERT INTO " + l_test_name + " (name) VALUES " + "(" + n.out + ");"
			end
			create l_statement.make (db, l_statement_text)
			l_statement.run_till_end
			assert ("exhausted", l_statement.after)

			l_statement.reset
			assert ("reset", not l_statement.after)

			l_statement.run_till_end

			l_query := db.query ("SELECT id from " + l_test_name + " order by id asc;", void, create {TUPLE [INTEGER]})
			l_inserted := l_query.list
			assert ("all inserted", l_inserted.count = 10)
			across
				1 |..| 10 is i
			loop
				assert (i.out + " inserted: " + l_inserted [i].id.out, l_inserted [i].id ~ i)
			end
		end

	when_single_statement_then_no_issue
		local
			l_statement: SCRIPT
			l_query: QUERY [TUPLE [id: INTEGER]]
			l_inserted: ARRAYED_LIST [TUPLE [id: INTEGER]]
		do
			create l_statement.make (db, "%NINSERT INTO " + test_name + " (name) VALUES ('1');")
			l_statement.run_till_end
			assert ("exhausted", l_statement.after)

			l_query := db.query ("SELECT id from " + test_name + " order by id asc;", void, create {TUPLE [INTEGER]})
			l_inserted := l_query.list
			assert ("all inserted", l_inserted.count = 1)

			l_statement.reset
			assert ("reset", not l_statement.after)
			l_statement.run_till_end
			assert ("exhausted again", l_statement.after)
			l_inserted := l_query.list
			assert ("all inserted", l_inserted.count = 2)
			across
				1 |..| 2 is i
			loop
				assert (i.out + " inserted: " + l_inserted [i].id.out, l_inserted [i].id ~ i)
			end
		end

end
