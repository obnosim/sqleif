note
	description: "Tests for PARAMETERIZED_STATEMENT"
	author: "obnosim"

class
	PARAMETERIZED_STATEMENT_TESTS

inherit

	BASE_TEST
		redefine
			on_prepare
		end

feature {NONE} -- Implementation

	on_prepare
		local
			l_test_name: STRING
		do
			precursor
			l_test_name := test_name
			db.execute ("CREATE TABLE " + l_test_name + " " + "[
				(
					name	TEXT NOT NULL PRIMARY KEY,
					value	INT
				);
			]")
		end

feature -- Tests

	when_custom_operand_types_configured_on_statement_then_correctly_converted
		local
			l_test_name: like test_name
			l_statement: PARAMETERIZED_STATEMENT [TUPLE [name: DEVELOPER_EXCEPTION; value: detachable BOUNDED [HASHABLE]]]
			l_exception: DEVELOPER_EXCEPTION
			l_bounded: BOUNDED [HASHABLE]
		do
			l_test_name := test_name

			create l_exception
			l_exception.set_description (test_name)
			create {ARRAYED_LIST [IMMUTABLE_STRING_32]} l_bounded.make (23)

			create l_statement.make (db, "insert into " + test_name + " (name, value) values (?1, ?2)")

			l_statement.configure ({DEVELOPER_EXCEPTION}).store_as_text (agent  (ia_value: DEVELOPER_EXCEPTION): C_STRING
				do
					create result.make (if attached ia_value.description as ila_desc then ila_desc else {STRING_32} "" end)
				end).done

			l_statement.configure ({BOUNDED [HASHABLE]}).store_as_integer_32 (agent {BOUNDED [HASHABLE]}.capacity).done

			l_statement ([l_exception, l_bounded])

			check attached db.query ("select name, value from " + l_test_name + " where name = ?;", [l_test_name], create {TUPLE [name: IMMUTABLE_STRING_32; value: INTEGER]}).single_or_default as la_retrieved then
				assert ("exact type correctly deserialized", la_retrieved.name ~ l_test_name.to_string_32)
				assert ("covariant type correctly deserialized", la_retrieved.value = 23)
			end
		end

	when_custom_operand_types_configured_on_database_then_correctly_converted
		local
			l_test_name: like test_name
			l_statement: PARAMETERIZED_STATEMENT [TUPLE [name: DEVELOPER_EXCEPTION; value: detachable BOUNDED [HASHABLE]]]
			l_exception: DEVELOPER_EXCEPTION
			l_bounded: BOUNDED [HASHABLE]
		do
			l_test_name := test_name

			create l_exception
			l_exception.set_description (test_name)
			create {ARRAYED_LIST [IMMUTABLE_STRING_32]} l_bounded.make (23)

			db.configure ({DEVELOPER_EXCEPTION}).store_as_text (agent  (ia_value: DEVELOPER_EXCEPTION): C_STRING
				do
					create result.make (if attached ia_value.description as ila_desc then ila_desc else {STRING_32} "" end)
				end).done

			db.configure ({BOUNDED [HASHABLE]}).store_as_integer_32 (agent {BOUNDED [HASHABLE]}.capacity).done

			create l_statement.make (db, "insert into " + test_name + " (name, value) values (?1, ?2)")
			l_statement ([l_exception, l_bounded])

			check attached db.query ("select name, value from " + l_test_name + " where name = ?;", [l_test_name], create {TUPLE [name: IMMUTABLE_STRING_32; value: INTEGER]}).single_or_default as la_retrieved then
				assert ("exact type correctly deserialized", la_retrieved.name ~ l_test_name.to_string_32)
				assert ("covariant type correctly deserialized", la_retrieved.value = 23)
			end
		end

	when_custom_operand_types_configured_globally_then_correctly_converted
		local
			l_test_name: like test_name
			l_statement: PARAMETERIZED_STATEMENT [TUPLE [name: DEVELOPER_EXCEPTION; value: detachable BOUNDED [HASHABLE]]]
			l_exception: DEVELOPER_EXCEPTION
			l_bounded: BOUNDED [HASHABLE]
		do
			l_test_name := test_name

			create l_exception
			l_exception.set_description (test_name)
			create {ARRAYED_LIST [IMMUTABLE_STRING_32]} l_bounded.make (23)

			{CONFIGURATION}.configure ({DEVELOPER_EXCEPTION}).store_as_text (agent  (ia_value: DEVELOPER_EXCEPTION): C_STRING
				do
					create result.make (if attached ia_value.description as ila_desc then ila_desc else {STRING_32} "" end)
				end).done

			{CONFIGURATION}.configure ({BOUNDED [HASHABLE]}).store_as_integer_32 (agent {BOUNDED [HASHABLE]}.capacity).done

			create l_statement.make (db, "insert into " + test_name + " (name, value) values (?1, ?2)")
			l_statement ([l_exception, l_bounded])

			check attached db.query ("select name, value from " + l_test_name + " where name = ?;", [l_test_name], create {TUPLE [name: IMMUTABLE_STRING_32; value: INTEGER]}).single_or_default as la_retrieved then
				assert ("exact type correctly deserialized", la_retrieved.name ~ l_test_name.to_string_32)
				assert ("covariant type correctly deserialized", la_retrieved.value = 23)
			end
		end

end
