﻿note
	description: "Tests for PARAMETERIZED_QUERY"
	author: "obnosim"

class
	PARAMETERIZED_QUERY_TESTS

inherit

	BASE_TEST
		redefine
			on_prepare
		end

feature {NONE} -- Implementation

	on_prepare
		local
			l_test_name: STRING
			l_insert: PARAMETERIZED_STATEMENT [STRING, detachable BOOLEAN_REF]
		do
			precursor
			l_test_name := test_name
			db.execute ("CREATE TABLE " + l_test_name + " " + "[
				(
					name	TEXT NOT NULL PRIMARY KEY,
					value	INT
				);
			]")
			create l_insert.make (db, "INSERT INTO " + l_test_name + " (name, value) VALUES (?1, ?2);")
			l_insert ("a", true)
			l_insert ("b", false)
			l_insert ("c", ({BOOLEAN_REF}).default_detachable_value)
			l_insert ("d", true)
		end

feature -- Execution tests

	when_single_operand_scalar_query_then_works
		local
			l_query: PARAMETERIZED_QUERY [TUPLE [value: BOOLEAN], TUPLE [rowcount: INTEGER]]
		do
			create l_query.make (db, "select count (*) from " + test_name + " where value = ?1;")
			assert ("correct result", l_query.scalar (true).rowcount = 2)
		end

	when_single_operand_single_column_query_then_works
		local
			l_query: PARAMETERIZED_QUERY [TUPLE [value: BOOLEAN], TUPLE [name: STRING]]
			l_result: ARRAYED_LIST [TUPLE [name: STRING]]
		do
			create l_query.make (db, "select name from " + test_name + " where value = ?1 order by name asc;")
			l_result := l_query.list (true)
			assert ("fully read", l_query.exhausted)
			assert ("2 results", l_result.count = 2)
			assert ("a: " + l_result.first.name, l_result.first.name ~ "a")
			assert ("d: " + l_result.last.name, l_result.last.name ~ "d")
		end

	when_single_operand_multi_column_query_then_works
		local
			l_query: PARAMETERIZED_QUERY [TUPLE [value: BOOLEAN], TUPLE [name: STRING; value: BOOLEAN]]
			l_result: ARRAYED_LIST [TUPLE [name: STRING; value: BOOLEAN]]
		do
			create l_query.make (db, "select name, value from " + test_name + " where value = ?1 order by name asc;")
			l_result := l_query.list (true)
			assert ("fully read", l_query.exhausted)
			assert ("2 results", l_result.count = 2)
			assert ("a: " + l_result.first.name, {TUPLE_UTIL}.equal_items (l_result.first, ["a", true]))
			assert ("d: " + l_result.last.name, {TUPLE_UTIL}.equal_items (l_result.last, ["d", true]))
		end

	when_multi_operand_multi_column_query_then_works
		local
			l_query: PARAMETERIZED_QUERY [TUPLE [name: STRING; value: BOOLEAN], TUPLE [name: STRING; value: BOOLEAN]]
			l_result: TUPLE [name: STRING; value: BOOLEAN]
		do
			create l_query.make (db, "select name, value from " + test_name + " where name = ?1 and value = ?2;")
			l_result := l_query.single ("b", false)
			assert ("correct result", {TUPLE_UTIL}.equal_items (l_result, ["b", false]))
			assert ("correct result (name)", l_result.name ~ "b")
			assert ("correct result (value)", not l_result.value)
		end

	when_no_result_then_works
		local
			l_query: PARAMETERIZED_QUERY [TUPLE [name: STRING; value: BOOLEAN], TUPLE [name: STRING; value: BOOLEAN]]
			l_result: TUPLE [name: STRING; value: BOOLEAN]
		do
			create l_query.make (db, "select name, value from " + test_name + " where name = ?1 and value = ?2;")
			l_result := l_query.single_or_default ("b", true)
			assert ("correct result", l_query.exhausted and not attached l_result)
			assert ("empty list", l_query.list ("b", true).is_empty)
		end

	when_nullable_column_then_works
		local
			l_query: PARAMETERIZED_QUERY [TUPLE [value: BOOLEAN], TUPLE [name: STRING; value: detachable BOOLEAN_REF]]
			l_result: ARRAYED_LIST [TUPLE [name: STRING; value: detachable BOOLEAN_REF]]
		do
			create l_query.make (db, "select name, value from " + test_name + " where ?1 = ?1 order by name asc;")
			l_result := l_query.list (true)
			assert ("all read: " + l_result.count.out, l_query.exhausted and l_result.count = 4)
			assert ("a: " + l_result [1].name, {TUPLE_UTIL}.equal_items (l_result [1], ["a", true]))
			assert ("b: " + l_result [2].name, {TUPLE_UTIL}.equal_items (l_result [2], ["b", false]))
			assert ("c: " + l_result [3].name, {TUPLE_UTIL}.equal_items (l_result [3], ["c", void]))
			assert ("d: " + l_result [4].name, {TUPLE_UTIL}.equal_items (l_result [4], ["d", true]))
		end

	when_persisted_as_blob_then_serialized_and_restored_correctly
		local
			l_original_value: ARRAY [INTEGER]
			l_retrieved: ANY
			l_insert: PARAMETERIZED_STATEMENT [STRING, CONTAINER [detachable INTEGER_REF]]
			l_query: PARAMETERIZED_QUERY [TUPLE [STRING], TUPLE [v: FINITE [INTEGER]]]
		do
			l_original_value := <<1, 2, 3, 4, 5>>
			create l_insert.make (db, "insert into " + test_name + " (name, value) values (?, ?);")
			l_insert.configure ({CONTAINER [BOOLEAN]}).store_as_blob
			l_insert ("test_blob", l_original_value)

			create l_query.make (db, "select value from " + test_name + " where name = ?")
			l_retrieved := l_query.single ("test_blob").v
			assert ("correctly stored and retrieved: %N" + l_original_value.out + "%N" + l_retrieved.out, l_retrieved.is_deep_equal (l_original_value))
		end

end
