note
	description: "Tests for STATEMENT"
	author: "obnosim"

class
	STATEMENT_TESTS

inherit

	BASE_TEST
		redefine
			on_prepare
		end

feature {NONE} -- Implementation

	on_prepare
		local
			l_test_name: STRING
		do
			precursor
			l_test_name := test_name
			db.execute ("CREATE TABLE " + l_test_name + " " + "[
				(
					name	TEXT NOT NULL PRIMARY KEY
				);
			]")
		end

feature -- Execution tests

	when_single_instruction_then_works
		local
			l_statement: STATEMENT
			l_inserted: ARRAYED_LIST [TUPLE [name: STRING]]
		do
			create l_statement.make (db, "%NINSERT INTO " + test_name + " (name) VALUES " + "('test');")
			l_statement.step

			l_inserted := db.query ("SELECT name from " + test_name + " order by name asc;", void, create {TUPLE [STRING]}).list
			assert ("executed", l_inserted.count = 1 and l_inserted.first.name ~ "test")
		end

	when_multiple_instructions_then_only_first_executed
		local
			l_test_name: STRING
			l_statement_text: STRING
			l_statement: STATEMENT
			l_inserted: ARRAYED_LIST [TUPLE [name: STRING]]
		do
			across
				1 |..| 5 is i
			from
				l_test_name := test_name
				l_statement_text := ""
			loop
				l_statement_text := l_statement_text + "%NINSERT INTO " + l_test_name + " (name) VALUES " + "(" + i.out + ");"
			end
			create l_statement.make (db, l_statement_text)
			l_statement.step

			l_inserted := db.query ("SELECT name from " + l_test_name + " order by name asc;", void, create {TUPLE [STRING]}).list
			assert ("only first statement executed", l_inserted.count = 1 and l_inserted.first.name ~ "1")
		end

end
