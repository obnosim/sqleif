note
	description: "Base test template"
	author: "obnosim"

deferred class
	BASE_TEST

inherit

	EQA_TEST_SET

	EQA_COMMONLY_USED_ASSERTIONS
		rename
			assert as common_assert
		undefine
			default_create
		end

feature {NONE} -- Implementation

	test_name: STRING_8
		do
			check attached environment.item (test_name_key) as la_qualified then
				result := la_qualified.split ('.').last.to_string_8
			end
		end

	database_path: STRING_32
		local
			l_directory, l_filename: STRING_32
		do
			l_directory := environment.item_attached (testing_directory_key, asserter)
			l_filename := environment.item_attached (test_name_key, asserter)
			result := file_system.build_path (l_directory + {OPERATING_ENVIRONMENT}.directory_separator.out.to_string_32 + l_filename + {STRING_32} ".sqlite", void)
		end

	db: DATABASE
		once ("object")
			create result.force_create_open_write (database_path)
		end

end
