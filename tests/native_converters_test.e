﻿note
	description: "Tests for DATABASE"
	author: "obnosim"

class
	NATIVE_CONVERTERS_TEST

inherit

	BASE_TEST

feature {NONE} -- Implementation

	test_table_row: TUPLE [ --
		boolean: BOOLEAN; d_boolean: detachable BOOLEAN_REF; --
		integer_8: INTEGER_8; d_integer_8: detachable INTEGER_8_REF; --
		integer_16: INTEGER_16; d_integer_16: detachable INTEGER_16_REF; --
		integer_32: INTEGER_32; d_integer_32: detachable INTEGER_32_REF; --
		integer_64: INTEGER_64; d_integer_64: detachable INTEGER_64_REF; --
		natural_8: NATURAL_8; d_natural_8: detachable NATURAL_8_REF; --
		natural_16: NATURAL_16; d_natural_16: detachable NATURAL_16_REF; --
		natural_32: NATURAL_32; d_natural_32: detachable NATURAL_32_REF; --
		natural_64: NATURAL_64; d_natural_64: detachable NATURAL_64_REF; --
		real: REAL_32; d_real: detachable REAL_REF; --
		double: REAL_64; d_double: detachable DOUBLE_REF; --
		character_8: CHARACTER_8; d_character_8: detachable CHARACTER_8_REF; --
		character_32: CHARACTER_32; d_character_32: detachable CHARACTER_32_REF; --
		string_8: STRING_8; d_string_8: detachable STRING_8; --
		immutable_string_8: IMMUTABLE_STRING_8; d_immutable_string_8: detachable IMMUTABLE_STRING_8; --
		readable_string_8: READABLE_STRING_8; d_readable_string_8: detachable READABLE_STRING_8; --
		string_32: STRING_32; d_string_32: detachable STRING_32; --
		immutable_string_32: IMMUTABLE_STRING_32; d_immutable_string_32: detachable IMMUTABLE_STRING_32; --
		readable_string_32: READABLE_STRING_32; d_readable_string_32: detachable READABLE_STRING_32; --
		string_general: STRING_GENERAL; d_string_general: detachable STRING_GENERAL; --
		immutable_string_general: IMMUTABLE_STRING_GENERAL; d_immutable_string_general: detachable IMMUTABLE_STRING_GENERAL; --
		readable_string_general: READABLE_STRING_GENERAL; d_readable_string_general: detachable READABLE_STRING_GENERAL; --
		c_string: C_STRING; d_c_string: detachable C_STRING; --
		blob: BLOB [ANY]; d_blob: detachable BLOB [ANY] --
		]
		do
			create result
		end

feature -- Tests

	when_persist_and_restore_natively_supported_type_then_works
		local
			l_insert_statement: STRING
			l_insert_args: like test_table_row
			l_original_blob_item: ARRAY [ANY]
		do
			db.execute ("CREATE TABLE native_converters " + "[
				(
					boolean						int		not null,	boolean_ref						int		null,
					
					integer_8					int		not null,	d_integer_8						int64	null,
					integer_16					int		not null,	d_integer_16					int64	null,
					integer_32					int		not null,	d_integer_32					int64	null,
					integer_64					int64	not null,	d_integer_64					int64	null,
					
					natural_8					int		not null,	d_natural_8						int64	null,
					natural_16					int		not null,	d_natural_16					int64	null,
					natural_32					int64	not null,	d_natural_32					int64	null,
					natural_64					int64	not null,	d_natural_64					int64	null,
					
					real						float	not null,	d_real							float	null,
					double						float	not null,	d_double						float	null,
					
					character_8					text	not null,	d_character_8					text	null,
					character_32				text	not null,	d_character_32					text	null,
					
					string_8					text	not null,	d_string_8						text	null,
					immutable_string_8			text	not null,	d_immutable_string_8			text	null,
					readable_string_8			text	not null,	d_readable_string_8				text	null,
					
					string_32					text	not null,	d_string_32						text	null,
					immutable_string_32			text	not null,	d_immutable_string_32			text	null,
					readable_string_32			text	not null,	d_readable_string_32			text	null,
					
					string_general				text	not null,	d_string_general				text	null,
					readable_string_general		text	not null,	d_readable_string_general		text	null,
					immutable_string_general	text	not null,	d_immutable_string_general		text	null,
					
					c_string					text	not null,	d_c_string						text	null,
					
					blob						blob	not null,	d_blob							blob	null
				);
			]")
			l_insert_statement := "[
					insert into native_converters (
						boolean,					boolean_ref,
					
						integer_8,					d_integer_8,
						integer_16,					d_integer_16,
						integer_32,					d_integer_32,
						integer_64,					d_integer_64,
						
						natural_8,					d_natural_8,
						natural_16,					d_natural_16,
						natural_32,					d_natural_32,
						natural_64,					d_natural_64,
						
						real,						d_real,
						double,						d_double,
						
						character_8,				d_character_8,
						character_32,				d_character_32,
						
						string_8,					d_string_8,
						immutable_string_8,			d_immutable_string_8,
						readable_string_8,			d_readable_string_8,
						
						string_32,					d_string_32,
						immutable_string_32,		d_immutable_string_32,
						readable_string_32,			d_readable_string_32,
						
						string_general,				d_string_general,
						readable_string_general,	d_readable_string_general,
						immutable_string_general,	d_immutable_string_general,
						
						c_string,					d_c_string,
					
						blob,						d_blob
					) values (
						?, ?,
						
						?, ?,
						?, ?,
						?, ?,
						?, ?,
						
						?, ?,
						?, ?,
						?, ?,
						?, ?,
						
						?, ?,
						?, ?,
						
						?, ?,
						?, ?,
						
						?, ?,
						?, ?,
						?, ?,
						
						?, ?,
						?, ?,
						?, ?,
						
						?, ?,
						?, ?,
						?, ?,
						
						?, ?,
						
						?, ?
					);
				]"
			create l_insert_args
			l_insert_args.boolean := true; l_insert_args.d_boolean := void

			l_insert_args.integer_8 := {INTEGER_8}.min_value
			l_insert_args.integer_16 := {INTEGER_16}.min_value
			l_insert_args.integer_32 := {INTEGER_32}.min_value
			l_insert_args.integer_64 := {INTEGER_64}.min_value

			l_insert_args.natural_8 := {NATURAL_8}.max_value
			l_insert_args.natural_16 := {NATURAL_16}.max_value
			l_insert_args.natural_32 := {NATURAL_32}.max_value
			l_insert_args.natural_64 := {NATURAL_64}.max_value

			l_insert_args.real := {REAL}.max_value
			l_insert_args.double := {DOUBLE}.max_value

			l_insert_args.character_8 := 'a'
			l_insert_args.character_32 := '🂺'

			l_insert_args.string_8 := {STRING_8} "string_8"
			l_insert_args.immutable_string_8 := {IMMUTABLE_STRING_8} "immutable_string_8"
			l_insert_args.readable_string_8 := {STRING_8} "readable_string_8"

			l_insert_args.string_32 := {STRING_32} "string_32 ä"
			l_insert_args.immutable_string_32 := {IMMUTABLE_STRING_32} "immutable_string_32 Ю"
			l_insert_args.readable_string_32 := {STRING_32} "readable_string_32 ⴳ"

			l_insert_args.string_general := {STRING_8} "string_general"
			l_insert_args.immutable_string_general := {IMMUTABLE_STRING_32} "immutable_string_general ٯ"
			l_insert_args.readable_string_general := {STRING_32} "readable_string_general Ю"

			l_insert_args.c_string := create {C_STRING}.make ({STRING_8} "c_string")

			l_original_blob_item := <<1, "one", '1'>>
			l_original_blob_item.compare_objects
			l_insert_args.blob := create {BLOB [ANY]}.write (l_original_blob_item)

			db.execute (l_insert_statement, l_insert_args)

			check attached db.query ("select * from native_converters;", void, test_table_row).single_or_default as la_retrieved then
				assert ("boolean restored", la_retrieved.boolean)

				assert ("integer_8 restored", la_retrieved.integer_8 ~ l_insert_args.integer_8)
				assert ("integer_16 restored", la_retrieved.integer_16 ~ l_insert_args.integer_16)
				assert ("integer_32 restored", la_retrieved.integer_32 ~ l_insert_args.integer_32)
				assert ("integer_64 restored", la_retrieved.integer_64 ~ l_insert_args.integer_64)

				assert ("natural_8 restored", la_retrieved.natural_8 ~ l_insert_args.natural_8)
				assert ("natural_16 restored", la_retrieved.natural_16 ~ l_insert_args.natural_16)
				assert ("natural_32 restored", la_retrieved.natural_32 ~ l_insert_args.natural_32)
				assert ("natural_64 restored", la_retrieved.natural_64 ~ l_insert_args.natural_64)

				assert ("real restored", la_retrieved.real ~ l_insert_args.real)
				assert ("double restored", la_retrieved.double ~ l_insert_args.double)

				assert ("character_8 restored", la_retrieved.character_8 ~ l_insert_args.character_8)
				assert ("character_32 restored", la_retrieved.character_32 ~ l_insert_args.character_32)

				assert ("string_8 restored", la_retrieved.string_8 ~ l_insert_args.string_8)
				assert ("immutable_string_8 restored", la_retrieved.immutable_string_8 ~ l_insert_args.immutable_string_8)
				assert ("readable_string_8 restored", la_retrieved.readable_string_8 ~ l_insert_args.readable_string_8)

				assert ("string_32 restored", la_retrieved.string_32.same_string (l_insert_args.string_32))
				assert ("immutable_string_32 restored", la_retrieved.immutable_string_32.same_string (l_insert_args.immutable_string_32))
				assert ("readable_string_32 restored", la_retrieved.readable_string_32.same_string (l_insert_args.readable_string_32))

				assert ("string_general restored", la_retrieved.string_general.same_string (l_insert_args.string_general))
				assert ("immutable_string_general restored", la_retrieved.immutable_string_general.same_string (l_insert_args.immutable_string_general))
				assert ("readable_string_general restored", la_retrieved.readable_string_general.same_string (l_insert_args.readable_string_general))

				assert ("c_string restored: '" + la_retrieved.c_string.string + "' vs '" + l_insert_args.c_string.string + "'", la_retrieved.c_string.string.same_string (l_insert_args.c_string.string))

				check attached {ARRAY [ANY]} la_retrieved.blob.item as la_retrieved_blob then
					assert_arrays_equal ("blob restored", la_retrieved_blob, l_original_blob_item)
				end
			end

		end

end
