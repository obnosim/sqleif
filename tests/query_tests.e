note
	description: "Tests for QUERY"
	author: "obnosim"

class
	QUERY_TESTS

inherit

	BASE_TEST
		redefine
			on_prepare
		end

feature {NONE} -- Implementation

	on_prepare
		local
			l_test_name: STRING
			l_insert: PARAMETERIZED_STATEMENT [STRING, detachable BOOLEAN_REF]
		do
			precursor
			l_test_name := test_name
			db.execute ("CREATE TABLE " + l_test_name + " " + "[
				(
					name	TEXT NOT NULL PRIMARY KEY,
					value	INT
				);
			]")
			create l_insert.make (db, "INSERT INTO " + l_test_name + " (name, value) VALUES (?1, ?2);")
			l_insert ("a", true)
			l_insert ("b", false)
			l_insert ("c", ({BOOLEAN_REF}).default_detachable_value)
			l_insert ("d", true)
		end

feature -- Execution tests

	when_scalar_query_then_works
		local
			l_query: QUERY [TUPLE [rowcount: INTEGER]]
		do
			create l_query.make (db, "select count (*) from " + test_name + " where value = 1;")
			assert ("correct result", l_query.scalar.rowcount = 2)
		end

	when_single_column_query_then_works
		local
			l_query: QUERY [TUPLE [name: STRING]]
			l_result: ARRAYED_LIST [TUPLE [name: STRING]]
		do
			create l_query.make (db, "select name from " + test_name + " where value = 1 order by name asc;")
			l_result := l_query.list
			assert ("fully read", l_query.exhausted)
			assert ("2 results", l_result.count = 2)
			assert ("a: " + l_result.first.name, l_result.first.name ~ "a")
			assert ("d: " + l_result.last.name, l_result.last.name ~ "d")
		end

	when_multi_column_query_then_works
		local
			l_query: QUERY [TUPLE [name: STRING; value: BOOLEAN]]
			l_result: ARRAYED_LIST [TUPLE [name: STRING; value: BOOLEAN]]
		do
			create l_query.make (db, "select name, value from " + test_name + " where value = 1 order by name asc;")
			l_result := l_query.list
			assert ("fully read", l_query.exhausted)
			assert ("2 results", l_result.count = 2)
			assert ("a: " + l_result.first.name, {TUPLE_UTIL}.equal_items (l_result.first, ["a", true]))
			assert ("d: " + l_result.last.name, {TUPLE_UTIL}.equal_items (l_result.last, ["d", true]))
		end

	when_no_result_then_works
		local
			l_query: QUERY [TUPLE [name: STRING; value: BOOLEAN]]
			l_result: TUPLE [name: STRING; value: BOOLEAN]
		do
			create l_query.make (db, "select name, value from " + test_name + " where name = 'b' and value = 1")
			l_result := l_query.single_or_default
			assert ("correct result", l_query.exhausted and not attached l_result)
			assert ("empty list", l_query.list.is_empty)
		end

	when_nullable_column_then_works
		local
			l_query: QUERY [TUPLE [name: STRING; value: detachable BOOLEAN_REF]]
			l_result: ARRAYED_LIST [TUPLE [name: STRING; value: detachable BOOLEAN_REF]]
		do
			create l_query.make (db, "select name, value from " + test_name + " order by name asc;")
			l_result := l_query.list
			assert ("all read", l_query.exhausted and l_result.count = 4)
			assert ("a: " + l_result [1].name, {TUPLE_UTIL}.equal_items (l_result [1], ["a", true]))
			assert ("a: " + l_result [2].name, {TUPLE_UTIL}.equal_items (l_result [2], ["b", false]))
			assert ("a: " + l_result [3].name, {TUPLE_UTIL}.equal_items (l_result [3], ["c", void]))
			assert ("a: " + l_result [4].name, {TUPLE_UTIL}.equal_items (l_result [4], ["d", true]))
		end

	when_custom_column_types_configured_on_query_then_correctly_converted
		local
			l_test_name: like test_name
			l_query: QUERY [TUPLE [name: DEVELOPER_EXCEPTION; value: detachable BOUNDED [HASHABLE]]]
		do
			l_test_name := test_name
			db.execute ("insert into " + l_test_name + " (name, value) values (?1, ?2)", [l_test_name, 23])
			
			create l_query.make (db, "select name, value from " + l_test_name + " where name = '" + l_test_name + "';")

			l_query.configure ({DEVELOPER_EXCEPTION}).store_as_text (agent  (ia_value: DEVELOPER_EXCEPTION): C_STRING
				do
					create result.make (if attached ia_value.description as ila_desc then ila_desc else {STRING_32} "" end)
				end).hydrate (agent  (ia_retrieved: C_STRING): DEVELOPER_EXCEPTION
				do
					create result
					result.set_description (create {STRING_32}.make_from_c (ia_retrieved.item))
				end)

			l_query.configure ({BOUNDED [HASHABLE]}).store_as_integer_32 (agent {BOUNDED [HASHABLE]}.capacity)
			.hydrate (agent  (ia_retrieved: INTEGER_64): ARRAYED_LIST [IMMUTABLE_STRING_32]
				do
					create result.make (ia_retrieved.max (0).min ({INTEGER}.max_value).as_integer_32)
				end)

			check attached l_query.single_or_default as la_retrieved then
				assert ("exact type correctly deserialized", la_retrieved.name.description ~ l_test_name.to_string_32)
				assert ("covariant type correctly deserialized", attached {ARRAYED_LIST [IMMUTABLE_STRING_32]} la_retrieved.value as la_bounded and then la_bounded.capacity = 23)
			end
		end

	when_custom_column_types_configured_on_database_then_correctly_converted
		local
			l_test_name: like test_name
			l_query: QUERY [TUPLE [name: DEVELOPER_EXCEPTION; value: detachable BOUNDED [HASHABLE]]]
		do
			l_test_name := test_name
			db.execute ("insert into " + test_name + " (name, value) values (?1, ?2)", [test_name, 23])

			db.configure ({DEVELOPER_EXCEPTION}).store_as_text (agent  (ia_value: DEVELOPER_EXCEPTION): C_STRING
				do
					create result.make (if attached ia_value.description as ila_desc then ila_desc else {STRING_32} "" end)
				end).hydrate (agent  (ia_retrieved: C_STRING): DEVELOPER_EXCEPTION
				do
					create result
					result.set_description (create {STRING_32}.make_from_c (ia_retrieved.item))
				end)

			db.configure ({BOUNDED [HASHABLE]}).store_as_integer_32 (agent {BOUNDED [HASHABLE]}.capacity)
			.hydrate (agent  (ia_retrieved: INTEGER_64): ARRAYED_LIST [IMMUTABLE_STRING_32]
				do
					create result.make (ia_retrieved.max (0).min ({INTEGER}.max_value).as_integer_32)
				end)

			create l_query.make (db, "select name, value from " + l_test_name + " where name = '" + l_test_name + "';")

			check attached l_query.single_or_default as la_retrieved then
				assert ("exact type correctly deserialized", la_retrieved.name.description ~ l_test_name.to_string_32)
				assert ("covariant type correctly deserialized", attached {ARRAYED_LIST [IMMUTABLE_STRING_32]} la_retrieved.value as la_bounded and then la_bounded.capacity = 23)
			end
		end

	when_custom_column_types_configured_globally_then_correctly_converted
		local
			l_test_name: like test_name
			l_query: QUERY [TUPLE [name: DEVELOPER_EXCEPTION; value: detachable BOUNDED [HASHABLE]]]
		do
			l_test_name := test_name
			db.execute ("insert into " + test_name + " (name, value) values (?1, ?2)", [test_name, 23])

			{CONFIGURATION}.configure ({DEVELOPER_EXCEPTION}).store_as_text (agent  (ia_value: DEVELOPER_EXCEPTION): C_STRING
				do
					create result.make (if attached ia_value.description as ila_desc then ila_desc else {STRING_32} "" end)
				end).hydrate (agent  (ia_retrieved: C_STRING): DEVELOPER_EXCEPTION
				do
					create result
					result.set_description (create {STRING_32}.make_from_c (ia_retrieved.item))
				end)

			{CONFIGURATION}.configure ({BOUNDED [HASHABLE]}).store_as_integer_32 (agent {BOUNDED [HASHABLE]}.capacity)
			.hydrate (agent  (ia_retrieved: INTEGER_64): ARRAYED_LIST [IMMUTABLE_STRING_32]
				do
					create result.make (ia_retrieved.max (0).min ({INTEGER}.max_value).as_integer_32)
				end)

			create l_query.make (db, "select name, value from " + l_test_name + " where name = '" + l_test_name + "';")

			check attached l_query.single_or_default as la_retrieved then
				assert ("exact type correctly deserialized", la_retrieved.name.description ~ l_test_name.to_string_32)
				assert ("covariant type correctly deserialized", attached {ARRAYED_LIST [IMMUTABLE_STRING_32]} la_retrieved.value as la_bounded and then la_bounded.capacity = 23)
			end
		end

end
